package com.arc.projects.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.PublishingPage;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class PublishingScenarioPage {
	
	
	
	public static WebDriver driver;
    
    ProjectsLoginPage projectsLoginPage;
    ProjectDashboardPage projectDashboardPage;
    PublishingPage publishingPage;
   	FolderPage folderPage;
   	Generate_Random_Number generate_Random_Number;
   	    
   public static String uName = PropertyReader.getProperty("Username");
   public static String pWord = PropertyReader.getProperty("Password");	   
   public static String FolderName = PropertyReader.getProperty("FolderName");	   
   public static String FullName = PropertyReader.getProperty("fullName");
   public static String state = PropertyReader.getProperty("state");	  
   public static String phoneNumber = PropertyReader.getProperty("phoneNumber");
   public static String password = PropertyReader.getProperty("password");
   
    
   
    
    
    @Parameters("browser")
    @BeforeMethod
    public WebDriver beforeTest(String browser) {
        
        if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        File dest = new File("./drivers/win/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
        ChromeOptions options = new ChromeOptions();
         options.addArguments("--start-maximized");
        options.setExperimentalOption("prefs", prefs);
        driver = new ChromeDriver( options );
        driver.get(PropertyReader.getProperty("SkysiteProdURL"));
 
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
               System.setProperty("webdriver.safari.noinstall", "false"); //To stop uninstall each time
               driver = new SafariDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   return driver;
 }



 /** TC_1 (Publishing Module Test-Cases):Verification Of Sheet Number After Uploading the file . 
  *  Scripted By Ranjan P
  *  
 * @throws Exception
 */
 @Test(priority = 0, enabled = false, description = "Verification Of Sheet Number After Uploading the file ")
 public void Verify_SheetNumber_Updation() throws Exception
 {
	 
 	try
 	{
 		
 		String Prj_Name = generate_Random_Number.ProjectName();
 		Log.testCaseInfo("TC_1(Publishing Page): Verification Of Sheet Number After Uploading the file ");
 	    //========Login Entry Into The Application=================================     		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
  		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);   		
    	folderPage=projectDashboardPage.createProject(Prj_Name);
		folderPage.New_Folder_Create(FolderName);
		folderPage.Select_Folder(FolderName);
		String Req_FileCount = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(Req_FileCount);
	  	String FolderPath = PropertyReader.getProperty("Publishing_Upload_TestData_Path");	  
		//publishingPage = folderPage.UploadFiles_WithIndexing(FolderPath, FileCount);
		
		//folderPage = new FolderPage(driver).get();
  		publishingPage = projectDashboardPage.SimpleExecution();
		publishingPage.MultipleRowValidationwithExcelSheet(); ;
  		
  		
  		
 		
 		
 	}
     catch(Exception e)
     {
     	e.getCause();
     	Log.exception(e, driver);
     }
 	finally
 	{
 		Log.endTestCase();
 		//driver.quit();
 	}
 }
 
 
 
 
 
 

/*  *//** TC_2 (Publishing Module Test-Cases):Verification Of Multiple Sheet Number After Uploading the file . 
  *  Scripted By Ranjan P
  *  
 * @throws Exception
 *//*
 @Test(priority = 0, enabled = false, description = "Verification Multiple Sheet Number After Uploading the file ")
 public void Verify_MultipleSheet_Validation() throws Exception
 {
	 
 	try
 	{
 		
 		String Prj_Name = generate_Random_Number.ProjectName();
 		Log.testCaseInfo("TC_1(Publishing Page): Verification Of Sheet Number After Uploading the file ");
 	    //========Login Entry Into The Application=================================     		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
  		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);   		
    	folderPage=projectDashboardPage.createProject(Prj_Name);
		folderPage.New_Folder_Create(FolderName);
		folderPage.Select_Folder(FolderName);
		String Req_FileCount = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(Req_FileCount);
	  	String FolderPath = PropertyReader.getProperty("Publishing_Upload_TestData_Path");	  
		publishingPage = folderPage.UploadFiles_WithIndexing(FolderPath, FileCount);
		
		folderPage = new FolderPage(driver).get();
  		//projectDashboardPage.SimpleExecution();
		
		//publishingPage.ValidationwithExcelSheet() ;
  		
  		
  		
 		
 		
 	}
     catch(Exception e)
     {
     	e.getCause();
     	Log.exception(e, driver);
     }
 	finally
 	{
 		Log.endTestCase();
 		//driver.quit();
 	}
 }*/
 
 
}
