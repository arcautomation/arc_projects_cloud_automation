package com.arc.projects.testscripts;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectAndFolder_Level_Search;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.ViewerScreenPage;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class ViewerScreen_RFI_Testcases {
	 

		public static WebDriver driver;	    
		ProjectsLoginPage projectsLoginPage;
	    ProjectDashboardPage projectDashboardPage;   
	    ViewerScreenPage viewerScreenPage; 
	    FolderPage folderPage;
	    ProjectAndFolder_Level_Search projectAndFolder_Level_Search;
	    
	    @Parameters("browser")
	    @BeforeMethod
        public WebDriver beforeTest(String browser) {
	        
	    	if(browser.equalsIgnoreCase("firefox")) 
	        {
	               File dest = new File("./drivers/win/geckodriver.exe");
	               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
	               driver = new FirefoxDriver();
	               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	        }
	        
	        else if (browser.equalsIgnoreCase("chrome")) 
	        { 
	        File dest = new File("./drivers/win/chromedriver.exe");
	        System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
	        Map<String, Object> prefs = new HashMap<String, Object>();
	        prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
	        ChromeOptions options = new ChromeOptions();
	         options.addArguments("--start-maximized");
	        options.setExperimentalOption("prefs", prefs);
	        driver = new ChromeDriver( options );
	        driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
	        } 
	        
	        else if (browser.equalsIgnoreCase("safari"))
	        {
	               System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
	               driver = new SafariDriver();
	               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	        }
	   return driver;
	 }
	    
	    
	    
	    
	    
	    
	    
	  //========================For RFI number further related cases need to validate the below ones as well

	    /*1) Create one fresh project.
	    2) In the RFI settings after creating project  change the:
	    a) RFI starting number to AB-0001
	    b) Due in 2days.
	    C)  Create 5 custom attributes.
	    Validation:
	    Steps 1: 
	    i) Login in the web and take the change set and observe the project available in the listings.
	    ii) Sync the project in web and move inside the RFI section.
	    iii) Verfiy the create RFI button should be available in the top right corner and in the blank section of the window when no RFI is available.

	    Steps 2a)
	    i) Try to create RFI, in the create RFI pop over window check the order of custom attributes it should be similar as provided in web.

	    Steps 2b and 2c:
	    i)  Create the RFI and check the RFI number it should be AB-0001
	    ii) The due days should be due in 2 days.
	    iii) Check the custom attributes it should be similar as provided in setttings section.
	    iv) Verify availability of comment field.
	    v) Check the RFI number in the listings as well including the status.


	    TestCase1: Starting  RFI with number(009)

	    */

	    @Test(priority = 0 , enabled = true, description = "Starting  RFI with number(009)")
	    public void StartingRFIwithNumber_009() throws Throwable
	         {         	 
	         try
	         {
	             Log.testCaseInfo("TC_01 (RFI Testcases):Starting  RFI with number(009)");
	             String uName = PropertyReader.getProperty("Username1");
	             String pWord = PropertyReader.getProperty("Password1");
	             String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
	             String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
	             String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
	             String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
	             String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
	             String RFINUMBER_009 = PropertyReader.getProperty("RFINUMBER_009");
	             String Project_Name = "Projet_"+Generate_Random_Number.generateRandomValue();
	             //==============Login with valid UserID/PassWord===========================
	             projectsLoginPage = new ProjectsLoginPage(driver).get();  
	             projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS	           		
	             //=========Create Project Randomly===================================          
	             folderPage=projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1, CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5,RFINUMBER_009);         		
	             //================File Upload without Index=====================
	             projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 	      		
	      		 projectAndFolder_Level_Search.ProjectManagement();	      		
	      		 projectAndFolder_Level_Search.RFI_CreationWithMultipleAttribute();
	      		 projectAndFolder_Level_Search.RFI_AttributeValidation();
	      	    
	    		 
	              	       			
	              		
	              	}
	                  catch(Exception e)
	                  {
	                  	e.getCause();
	                  	Log.exception(e, driver);
	                  }
	              	finally
	              	{
	              		Log.endTestCase();
	              		driver.close();
	              	}
	           	
	           }  

	          
	    /*1) Create one fresh project.
	    2) In the RFI settings after creating project  change the:
	    a) RFI starting number to AB-0001
	    b) Due in 2days.
	    C)  Create 5 custom attributes.
	    Validation:
	    Steps 1: 
	    i) Login in the web and take the change set and observe the project available in the listings.
	    ii) Sync the project in web and move inside the RFI section.
	    iii) Verfiy the create RFI button should be available in the top right corner and in the blank section of the window when no RFI is available.

	    Steps 2a)
	    i) Try to create RFI, in the create RFI pop over window check the order of custom attributes it should be similar as provided in web.

	    Steps 2b and 2c:
	    i)  Create the RFI and check the RFI number it should be AB-0001
	    ii) The due days should be due in 2 days.
	    iii) Check the custom attributes it should be similar as provided in setttings section.
	    iv) Verify availability of comment field.
	    v) Check the RFI number in the listings as well including the status.


	    TestCase2: Starting  RFI with number(999)

	    */

	    @Test(priority = 1 , enabled = true, description = "Starting RFI with number(999)")
	    public void StartingRFIwithnumber_999() throws Throwable
	         {         	 
	         try
	         {
	        	 Log.testCaseInfo("TC_01 (RFI Testcases):Starting  RFI with number(999)");
	             String uName = PropertyReader.getProperty("Username1");
	             String pWord = PropertyReader.getProperty("Password1");
	             String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
	             String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
	             String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
	             String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
	             String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
	             String RFINUMBER_999 = PropertyReader.getProperty("RFINUMBER_999");
	             String Project_Name = "Projet_"+Generate_Random_Number.generateRandomValue();
	             //==============Login with valid UserID/PassWord===========================
	             projectsLoginPage = new ProjectsLoginPage(driver).get();  
	             projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS	           		
	             //=========Create Project Randomly===================================          
	             folderPage=projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1, CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5,RFINUMBER_999);         		
	             //================File Upload without Index=====================
	             projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 	      		
	      		 projectAndFolder_Level_Search.ProjectManagement();	      		
	      		 projectAndFolder_Level_Search.RFI_CreationWithMultipleAttribute();
	      		 projectAndFolder_Level_Search.RFI_AttributeValidation();
	    		 
	    		 
	              	       			
	              		
	              	}
	                  catch(Exception e)
	                  {
	                  	e.getCause();
	                  	Log.exception(e, driver);
	                  }
	              	finally
	              	{
	              		Log.endTestCase();
	              		driver.close();
	              	}
	           	
	           }  
    
	    
	    
	    
	    
	    /*1) Create one fresh project.
	    2) In the RFI settings after creating project  change the:
	    a) RFI starting number to AB-0001
	    b) Due in 2days.
	    C)  Create 5 custom attributes.
	    Validation:
	    Steps 1: 
	    i) Login in the web and take the change set and observe the project available in the listings.
	    ii) Sync the project in web and move inside the RFI section.
	    iii) Verfiy the create RFI button should be available in the top right corner and in the blank section of the window when no RFI is available.

	    Steps 2a)
	    i) Try to create RFI, in the create RFI pop over window check the order of custom attributes it should be similar as provided in web.

	    Steps 2b and 2c:
	    i)  Create the RFI and check the RFI number it should be AB-0001
	    ii) The due days should be due in 2 days.
	    iii) Check the custom attributes it should be similar as provided in setttings section.
	    iv) Verify availability of comment field.
	    v) Check the RFI number in the listings as well including the status.


	    TestCase2: Starting  RFI with number(009)

	    */

	    @Test(priority = 2 , enabled = true , description = "Starting RFI with number(9999)")
	    public void StartingRFIwithnumber_9999() throws Throwable
	         {         	 
	         try
	         {
	        	 Log.testCaseInfo("TC_01 (RFI Testcases):Starting  RFI with number(9999)");
	             String uName = PropertyReader.getProperty("Username1");
	             String pWord = PropertyReader.getProperty("Password1");
	             String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
	             String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
	             String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
	             String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
	             String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
	             String RFINUMBER_9999 = PropertyReader.getProperty("RFINUMBER_9999");
	             String Project_Name = "Projet_"+Generate_Random_Number.generateRandomValue();
	             //==============Login with valid UserID/PassWord===========================
	             projectsLoginPage = new ProjectsLoginPage(driver).get();  
	             projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS	           		
	             //=========Create Project Randomly===================================          
	             folderPage=projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1, CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5,RFINUMBER_9999);         		
	             //================File Upload without Index=====================
	             projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 	      		
	      		 projectAndFolder_Level_Search.ProjectManagement();	      		
	      		 projectAndFolder_Level_Search.RFI_CreationWithMultipleAttribute();
	      		 projectAndFolder_Level_Search.RFI_AttributeValidation();
	    		 
	    		 
	              	       			
	              		
	              	}
	                  catch(Exception e)
	                  {
	                  	e.getCause();
	                  	Log.exception(e, driver);
	                  }
	              	finally
	              	{
	              		Log.endTestCase();
	              		driver.close();
	              	}
	           	
	           }   
	    
	    
	    
	    
	    /*1) Create one fresh project.
	    2) In the RFI settings after creating project  change the:
	    a) RFI starting number to AB-0001
	    b) Due in 2days.
	    C)  Create 5 custom attributes.
	    Validation:
	    Steps 1: 
	    i) Login in the web and take the change set and observe the project available in the listings.
	    ii) Sync the project in web and move inside the RFI section.
	    iii) Verfiy the create RFI button should be available in the top right corner and in the blank section of the window when no RFI is available.

	    Steps 2a)
	    i) Try to create RFI, in the create RFI pop over window check the order of custom attributes it should be similar as provided in web.

	    Steps 2b and 2c:
	    i)  Create the RFI and check the RFI number it should be AB-0001
	    ii) The due days should be due in 2 days.
	    iii) Check the custom attributes it should be similar as provided in setttings section.
	    iv) Verify availability of comment field.
	    v) Check the RFI number in the listings as well including the status.
	    TestCase2: Starting  RFI with number(009)

	    */

	    @Test(priority = 3 , enabled = true , description = "Starting RFI with number(9999999)")
	    public void StartingRFIwithnumber_9999999() throws Throwable
	         {         	 
	         try
	         {
	        	 Log.testCaseInfo("TC_01 (RFI Testcases):Starting  RFI with number(9999999)");
	             String uName = PropertyReader.getProperty("Username1");
	             String pWord = PropertyReader.getProperty("Password1");
	             String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
	             String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
	             String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
	             String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
	             String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
	             String RFINUMBER_9999999 = PropertyReader.getProperty("RFINUMBER_9999999");
	             String Project_Name = "Projet_"+Generate_Random_Number.generateRandomValue();
	             //==============Login with valid UserID/PassWord===========================
	             projectsLoginPage = new ProjectsLoginPage(driver).get();  
	             projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS	           		
	             //=========Create Project Randomly===================================          
	             folderPage=projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1, CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5,RFINUMBER_9999999);         		
	             //================File Upload without Index=====================
	             projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 	      		
	      		 projectAndFolder_Level_Search.ProjectManagement();	      		
	      		 projectAndFolder_Level_Search.RFI_CreationWithMultipleAttribute();
	      		 projectAndFolder_Level_Search.RFI_AttributeValidation();
	    		 
	    		 
	              	       			
	              		
	              	}
	                  catch(Exception e)
	                  {
	                  	e.getCause();
	                  	Log.exception(e, driver);
	                  }
	              	finally
	              	{
	              		Log.endTestCase();
	              		driver.close();
	              	}
	           	
	           }   
	    
	    
	    
	    
	    
	    
	    
	    
	    /*1) Create one fresh project.
	    2) In the RFI settings after creating project  change the:
	    a) RFI starting number to AB-0001
	    b) Due in 2days.
	    C)  Create 5 custom attributes.
	    Validation:
	    Steps 1: 
	    i) Login in the web and take the change set and observe the project available in the listings.
	    ii) Sync the project in web and move inside the RFI section.
	    iii) Verfiy the create RFI button should be available in the top right corner and in the blank section of the window when no RFI is available.

	    Steps 2a)
	    i) Try to create RFI, in the create RFI pop over window check the order of custom attributes it should be similar as provided in web.

	    Steps 2b and 2c:
	    i)  Create the RFI and check the RFI number it should be AB-0001
	    ii) The due days should be due in 2 days.
	    iii) Check the custom attributes it should be similar as provided in setttings section.
	    iv) Verify availability of comment field.
	    v) Check the RFI number in the listings as well including the status.
	    TestCase2: Starting  RFI with number(009)

	    */

	    @Test(priority = 4 , enabled = true , description = "Starting RFI No with Special Characterts")
	    public void RFInumberwithSpecialCharacters() throws Throwable
	         {         	 
	         try
	         {
	        	 Log.testCaseInfo("TC_01 (RFI Testcases):Starting RFI No with Special Characterts");
	             String uName = PropertyReader.getProperty("Username1");
	             String pWord = PropertyReader.getProperty("Password1");
	             String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
	             String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
	             String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
	             String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
	             String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
	             String RFINUMBER_Splcharacter = PropertyReader.getProperty("RFINUMBER_withSpecialCharacter");
	             String Project_Name = "Projet_"+Generate_Random_Number.generateRandomValue();
	             //==============Login with valid UserID/PassWord===========================
	             projectsLoginPage = new ProjectsLoginPage(driver).get();  
	             projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS	           		
	             //=========Create Project Randomly===================================          
	             folderPage=projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1, CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5,RFINUMBER_Splcharacter);         		
	             //================File Upload without Index=====================
	             projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 	      		
	      		 projectAndFolder_Level_Search.ProjectManagement();	      		
	      		 projectAndFolder_Level_Search.RFI_CreationWithMultipleAttribute();
	      		 projectAndFolder_Level_Search.RFI_AttributeValidation();
	    		 
	    		 
	              	       			
	              		
	              	}
	                  catch(Exception e)
	                  {
	                  	e.getCause();
	                  	Log.exception(e, driver);
	                  }
	              	finally
	              	{
	              		Log.endTestCase();
	              		driver.close();
	              	}
	           	
	           }   
	    
	    
	    
	    
	    /*1) Create one fresh project.
	    2) In the RFI settings after creating project  change the:
	    a) RFI starting number to AB-0001
	    b) Due in 2days.
	    C)  Create 5 custom attributes.
	    Validation:
	    Steps 1: 
	    i) Login in the web and take the change set and observe the project available in the listings.
	    ii) Sync the project in web and move inside the RFI section.
	    iii) Verfiy the create RFI button should be available in the top right corner and in the blank section of the window when no RFI is available.

	    Steps 2a)
	    i) Try to create RFI, in the create RFI pop over window check the order of custom attributes it should be similar as provided in web.

	    Steps 2b and 2c:
	    i)  Create the RFI and check the RFI number it should be AB-0001
	    ii) The due days should be due in 2 days.
	    iii) Check the custom attributes it should be similar as provided in setttings section.
	    iv) Verify availability of comment field.
	    v) Check the RFI number in the listings as well including the status.
	    TestCase2: Check Validation if Given special characters given before alphabets (-AB00001)

	    */

	   /* @Test(priority = 5 , enabled = true , description = "Check Validation if Given special characters given before alphabets ")
	    public void CheckValidationifGiven_special_characters_before_alphabets() throws Throwable
	         {         	 
	         try
	         {
	        	 Log.testCaseInfo("TC_01 (RFI )Testcases:Check Validation if Given special characters given before alphabets ");
	             String uName = PropertyReader.getProperty("Username1");
	             String pWord = PropertyReader.getProperty("Password1");
	             String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
	             String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
	             String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
	             String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
	             String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
	             String RFINUMBER_Splcharacter = PropertyReader.getProperty("RFINUMBER_withSpecialCharacter");
	             String Project_Name = "Projet_"+Generate_Random_Number.generateRandomValue();
	             //==============Login with valid UserID/PassWord===========================
	             projectsLoginPage = new ProjectsLoginPage(driver).get();  
	             projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS	           		
	             //=========Create Project Randomly===================================          
	             folderPage=projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1, CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5,RFINUMBER_Splcharacter);         		
	             //================File Upload without Index=====================
	             projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 	      		
	      		 projectAndFolder_Level_Search.ProjectManagement();	      		
	      		 projectAndFolder_Level_Search.RFI_CreationWithMultipleAttribute();
	      		 projectAndFolder_Level_Search.RFI_AttributeValidation();
	    		 
	    		 
	              	       			
	              		
	              	}
	                  catch(Exception e)
	                  {
	                  	e.getCause();
	                  	Log.exception(e, driver);
	                  }
	              	finally
	              	{
	              		Log.endTestCase();
	              		driver.close();
	              	}
	           	
	           }   */
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
   //=========== RFI related Testcases=======================================	   
   /* Testcase: 1. Delete 1st revision file from web where RFI is exist.Take the update change set in the device for deleted file.Verify the existance of RFI in the 2nd revision file. 
    * 1) RFI should be carry forward to second revision.
      2) Verify the position of RFI.
      3) Verify with all kind of RFI status(Open, close, re-open, responded, reject, void, closed)      
    */
          
@Test(priority = 6 , enabled = true, description = "Delete 1st revision file from web where RFI is exist.Take the update change set in the device for deleted file.Verify the existance of RFI in the 2nd revision file.")
public void DeleteFirstRFIfromWebCheking() throws Throwable
     {   
	  
     try
     {
         Log.testCaseInfo("TC_01 (RFI Delete Related Testcases):Delete 1st revision file from web where RFI is exist.Take the update change set in the device for deleted file.Verify the existance of RFI in the 2nd revision file.");
         String uName = PropertyReader.getProperty("Username1");
         String pWord = PropertyReader.getProperty("Password1");
        
         //==============Login with valid UserID/PassWord===========================
         projectsLoginPage = new ProjectsLoginPage(driver).get();  
         projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS	           		
         //=========Create Project Randomly===================================
         String Project_Name = "Projet_"+Generate_Random_Number.generateRandomValue();
         folderPage=projectDashboardPage.createProject(Project_Name); 
         //folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);
        // folderPage.SelectFolder(Foldername); 
         //=========Create Folder Randomly===================================
         String Foldername = "Folder_"+Generate_Random_Number.generateRandomValue();
         folderPage.New_Folder_Create(Foldername); 	
         folderPage.Select_Folder(Foldername);         		
         //================File Upload without Index=====================
         viewerScreenPage = new ViewerScreenPage(driver).get();
         String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
         String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
         int FileCount = Integer.parseInt(CountOfFilesInFolder);
         folderPage.Upload_WithoutIndex(FolderPath, FileCount);     		
         viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
         folderPage.UploadFiles_WithoutIndexMycomputer(FolderPath, FileCount);
         viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
         //viewerScreenPage.simpleexecutipn();
         //============RFI Creation ON first File===========================
         String parentHandle = driver.getWindowHandle();
		 viewerScreenPage.Image1();
		 viewerScreenPage.RFI_TestDataCreation_ownerLevel(parentHandle);
		 viewerScreenPage.RFI_OwnerLevel_validation();
		 driver.close();	
		 driver.switchTo().window(parentHandle);
		 /*String parentHandle1 = driver.getWindowHandle();
		 viewerScreenPage.Image1();		
		 viewerScreenPage.DeleteFirstRevisionfile(parentHandle1);		 
		 driver.switchTo().window(parentHandle1);	 
		 //===========RFI Creation Of Second Revision File==================		 
		  String parentHandle2 = driver.getWindowHandle();
     	  viewerScreenPage.Image1();
     	  viewerScreenPage.RFI_RevisionFile(parentHandle2);
     	  viewerScreenPage.RFI_OwnerLevel_validation();
     	  viewerScreenPage.StatusValidation();    	 
     	  driver.close();     	 
     	  driver.switchTo().window(parentHandle2);*/
     	  viewerScreenPage.Logout();	 
		 
			       			
          		
          	}
              catch(Exception e)
              {
              	e.getCause();
              	Log.exception(e, driver);
              }
          	finally
          	{
          		Log.endTestCase();
          		driver.close();
          	}
       	
       }  
      	  
     


/*Test case : 2.Now Delete 2nd revision file from web where RFI is exist.Take the update change set in 
 * the device for deleted file.File level RFI should converted to project level RFI.
 * 1) File level RFI should converted to project level RFI.
   2) Validate the user should be able to change the status of the RFI.
   3) The user should be able to comment in the RFI.
 */

@Test(priority = 7 , enabled = true, description = "File level RFI should converted to project level RFI.")
public void DeleteSecondRFI() throws Throwable
     {         	 
     try
     {
         Log.testCaseInfo("TC_02 (RFI Delete Related Testcases):File level RFI should converted to project level RFI.");
         String uName = PropertyReader.getProperty("Username1");
         String pWord = PropertyReader.getProperty("Password1");
         //==============Login with valid UserID/PassWord===========================
         projectsLoginPage = new ProjectsLoginPage(driver).get();  
         projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS	           		
         //=========Create Project Randomly===================================
         String Project_Name = "Projet_"+Generate_Random_Number.generateRandomValue();
         folderPage=projectDashboardPage.createProject(Project_Name);         		
         //=========Create Folder Randomly===================================
         String Foldername = "Folder_"+Generate_Random_Number.generateRandomValue();
         folderPage.New_Folder_Create(Foldername); 	
         folderPage.Select_Folder(Foldername);         		
         //================File Upload without Index=====================
         viewerScreenPage = new ViewerScreenPage(driver).get();
         String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
         String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
         int FileCount = Integer.parseInt(CountOfFilesInFolder);
         folderPage.Upload_WithoutIndex(FolderPath, FileCount);     		
         viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
         folderPage.UploadFiles_WithoutIndexMycomputer(FolderPath, FileCount);
         viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
         viewerScreenPage.Logout();
         //============RFI Creation ON first File===========================
         /*String parentHandle = driver.getWindowHandle();
		 viewerScreenPage.Image1();
		 Log.assertThat(viewerScreenPage.RFI_TestDataCreation_ownerLevel(parentHandle), "Test Data Creation working Successfully","Test Data Creation not working Successfully", driver);
		 Log.assertThat(viewerScreenPage.RFI_OwnerLevel_validation(), "Owner Level validation working Successfully","Owner Level validation not working Successfully", driver);
		 driver.close();
		 viewerScreenPage.Image1();
		 //viewerScreenPage.DeleteRFI();		 
		 driver.switchTo().window(parentHandle);
		 
		 //===========RFI Creation Of Second Revision File==================
		 
		  String parentHandle1 = driver.getWindowHandle();
     	  viewerScreenPage.Image2();
     	  viewerScreenPage.RFI_RevisionFile(parentHandle1);
     	  viewerScreenPage.RFI_OwnerLevel_validation();
     	  viewerScreenPage.MainStatus();
     	  SkySiteUtils.waitTill(5000);
     	  driver.close();
     	  SkySiteUtils.waitTill(5000);
     	  driver.switchTo().window(parentHandle1);
     	  viewerScreenPage.Logout();	 
		 */
		 // Log.assertThat( viewerScreenPage.Logout(), "Logout working Successfully","Logout not working Successfully", driver);

          	       			
          		
          	}
              catch(Exception e)
              {
              	e.getCause();
              	Log.exception(e, driver);
              }
          	finally
          	{
          		Log.endTestCase();
          		driver.close();
          	}
       	
       }  
      	  
     
/*Test case : 03.Delete 1st revision file from web where RFI is exist.Skip the update
 *  change set in the device for deleted file.Verify the existance of RFI in the 2nd revision file
 *  1) RFI should be carry forward to second revision.
    2) Verify the position of RFI.
    3) Verify with all kind of RFI status(Open, close, re-open, responded, reject, void, closed).
 */

@Test(priority = 8 , enabled = true, description = "RFI should be carry forward to second revision.")
public void Deletefirstfile() throws Throwable
     {         	 
     try
     {
         Log.testCaseInfo("TC_03 (RFI Delete Related Testcases):RFI should be carry forward to second revision.");
         String uName = PropertyReader.getProperty("Username1");
         String pWord = PropertyReader.getProperty("Password1");
         //==============Login with valid UserID/PassWord===========================
         projectsLoginPage = new ProjectsLoginPage(driver).get();  
         projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS	           		
         //=========Create Project Randomly===================================
         String Project_Name = "Projet_"+Generate_Random_Number.generateRandomValue();
         folderPage=projectDashboardPage.createProject(Project_Name);         		
         //=========Create Folder Randomly===================================
         String Foldername = "Folder_"+Generate_Random_Number.generateRandomValue();
         folderPage.New_Folder_Create(Foldername); 	
         folderPage.Select_Folder(Foldername);         		
         //================File Upload without Index=====================
         viewerScreenPage = new ViewerScreenPage(driver).get();
         String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
         String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
         int FileCount = Integer.parseInt(CountOfFilesInFolder);
         folderPage.Upload_WithoutIndex(FolderPath, FileCount);     		
         viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
         folderPage.UploadFiles_WithoutIndexMycomputer(FolderPath, FileCount);
         viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
         
         //============RFI Creation ON first File===========================
         String parentHandle = driver.getWindowHandle();
		 viewerScreenPage.Image1();
		 Log.assertThat(viewerScreenPage.RFI_TestDataCreation_ownerLevel(parentHandle), "Test Data Creation working Successfully","Test Data Creation not working Successfully", driver);
		 Log.assertThat(viewerScreenPage.RFI_OwnerLevel_validation(), "Owner Level validation working Successfully","Owner Level validation not working Successfully", driver);
		 driver.close();
		 viewerScreenPage.Image1();
		 //viewerScreenPage.DeleteRFI();		 
		 driver.switchTo().window(parentHandle);
		 viewerScreenPage.Logout();
		 
		 //===========RFI Creation Of Second Revision File==================
		 
		 /* String parentHandle1 = driver.getWindowHandle();
     	  viewerScreenPage.Image2();
     	  viewerScreenPage.RFI_RevisionFile(parentHandle1);
     	  viewerScreenPage.RFI_OwnerLevel_validation();
     	  viewerScreenPage.MainStatus();
     	  SkySiteUtils.waitTill(5000);
     	  driver.close();
     	  SkySiteUtils.waitTill(5000);
     	  driver.switchTo().window(parentHandle1);
     	  viewerScreenPage.Logout();*/	 
		 
		 // Log.assertThat( viewerScreenPage.Logout(), "Logout working Successfully","Logout not working Successfully", driver);

          	       			
          		
          	}
              catch(Exception e)
              {
              	e.getCause();
              	Log.exception(e, driver);
              }
          	finally
          	{
          		Log.endTestCase();
          		driver.close();
          	}
       	
       }  





/*Test case : 04.Navigate from RFI listings and try to create RFI annotation.
 * Create one in document level in online mode with To, Cc, Subject, disciple, sheet number, custom attributes, specification, potential cost impact(checked), potential schedule impact(checked), Question and attachment(external and internal)
      1) Navigate to the document and validate the document.
      2) Validate the postion and content of RFI.
      3) Close the pop over and tap on view all and validate the position of mark ups.
      4) Create RFI, Punch and photo annotation on the document an validate those in web.
 */

@Test(priority = 9 , enabled = true, description = "Navigate from RFI listings and try to create RFI")
public void NavigatefromRFIListingtocreateRFI() throws Throwable
     {         	 
     try
     {
         Log.testCaseInfo("TC_04 (RFI Related Testcases):Navigate from RFI listings and try to create RFI");
         String uName = PropertyReader.getProperty("Username1");
         String pWord = PropertyReader.getProperty("Password1");
         //==============Login with valid UserID/PassWord===========================
         projectsLoginPage = new ProjectsLoginPage(driver).get();  
         projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS	           		
         //=========Create Project Randomly===================================
         String Project_Name = "Projet_"+Generate_Random_Number.generateRandomValue();
         folderPage=projectDashboardPage.createProject(Project_Name);         		
         //=========Create Folder Randomly===================================
         String Foldername = "Folder_"+Generate_Random_Number.generateRandomValue();
         folderPage.New_Folder_Create(Foldername); 	
         folderPage.Select_Folder(Foldername);         		
         //================File Upload without Index=====================
         viewerScreenPage = new ViewerScreenPage(driver).get();
         String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
         String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
         int FileCount = Integer.parseInt(CountOfFilesInFolder);
         folderPage.Upload_WithoutIndex(FolderPath, FileCount);     		
         viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
          //============RFI Creation ON first File===========================
         String parentHandle = driver.getWindowHandle();
		 viewerScreenPage.Image1();
		 viewerScreenPage.RFITestDataCreation_potentialChecked(parentHandle);
		// Log.assertThat(viewerScreenPage.RFITestDataCreation_potentialChecked(parentHandle), "Test Data Creation working Successfully","Test Data Creation not working Successfully", driver);
		 Log.assertThat(viewerScreenPage.RFI_OwnerLevel_validation(), "Owner Level validation working Successfully","Owner Level validation not working Successfully", driver);
		 driver.close();
		 driver.switchTo().window(parentHandle);		 
		 viewerScreenPage.Image1();
		 viewerScreenPage.RFI_RevisionFile(parentHandle);		
		 Log.assertThat(viewerScreenPage.RFI_OwnerLevel_validation(), "Owner Level validation working Successfully","Owner Level validation not working Successfully", driver);
		 driver.close();
		 driver.switchTo().window(parentHandle);
     	 viewerScreenPage.Logout();	 		         		
          	
         }
              
         catch(Exception e)
              
          {
              	e.getCause();
              	Log.exception(e, driver);
          }
          	finally
          	
          {
          		Log.endTestCase();
          		driver.close();
          	}
       	
       }  
      	  
      

/*Test case : 05.Navigate from RFI listings and try to create PUNCH annotation.
 * Create one in document level in online mode with To, Cc, Subject, disciple, sheet number, custom attributes, specification, potential cost impact(checked), potential schedule impact(checked), Question and attachment(external and internal)
   1) Navigate to the document and validate the document.
   2) Validate the postion and content of RFI.
   3) Close the pop over and tap on view all and validate the position of mark ups.
   4) Create RFI, Punch and photo annotation on the document an validate those in web.
 */

@Test(priority = 10, enabled = true, description = "Navigate from RFI listings and try to create PUNCH")
public void NavigatefromRFIListingtocreatePunch() throws Throwable
     {         	 
     try
     {
         Log.testCaseInfo("TC_05 (RFI Related Testcases):Navigate from RFI listings and try to create PUNCH");
         String uName = PropertyReader.getProperty("Username1");
         String pWord = PropertyReader.getProperty("Password1");
         //==============Login with valid UserID/PassWord===========================
         projectsLoginPage = new ProjectsLoginPage(driver).get();  
         projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS	           		
         //=========Create Project Randomly===================================
         String Project_Name = "Projet_"+Generate_Random_Number.generateRandomValue();
         folderPage=projectDashboardPage.createProject(Project_Name);         		
         //=========Create Folder Randomly===================================
         String Foldername = "Folder_"+Generate_Random_Number.generateRandomValue();
         folderPage.New_Folder_Create(Foldername); 	
         folderPage.Select_Folder(Foldername);         		
         //================File Upload without Index=====================
         viewerScreenPage = new ViewerScreenPage(driver).get();
         String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
         String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
         int FileCount = Integer.parseInt(CountOfFilesInFolder);
         folderPage.Upload_WithoutIndex(FolderPath, FileCount);     		
         viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
        
         //============Punch Creation ON first File===========================
         String parentHandle = driver.getWindowHandle();
		 viewerScreenPage.Image1();
		 viewerScreenPage.PunchValidation_creation1(parentHandle);
		 viewerScreenPage.ValidationAfterPunchCreation();
		 driver.close();		 			 
		 driver.switchTo().window(parentHandle);		 
		 viewerScreenPage.Logout(); 
		  	       			
          		
          	}
              catch(Exception e)
              {
              	e.getCause();
              	Log.exception(e, driver);
              }
          	finally
          	{
          		Log.endTestCase();
          		driver.close();
          	}
       	
       }  
      	  
/*Test case : 06.Navigate from RFI listings and try to create Photo annotation.
 * Create one in document level in online mode with To, Cc, Subject, disciple, sheet number, custom attributes, specification, potential cost impact(checked), potential schedule impact(checked), Question and attachment(external and internal)
   1) Navigate to the document and validate the document.
   2) Validate the postion and content of RFI.
   3) Close the pop over and tap on view all and validate the position of mark ups.
   4) Create RFI, Punch and photo annotation on the document an validate those in web.
 */

@Test(priority = 11 , enabled = true, description = "Navigate from RFI listings and try to create Photo Annotation")
public void NavigatefromRFIListingtocreatePhotoAnnotation() throws Throwable
     {         	 
     try
     {
         Log.testCaseInfo("TC_06 (RFI Related Testcases):Navigate from RFI listings and try to create  Photo Annotation");
         String uName = PropertyReader.getProperty("Username1");
         String pWord = PropertyReader.getProperty("Password1");
         //==============Login with valid UserID/PassWord===========================
         projectsLoginPage = new ProjectsLoginPage(driver).get();  
         projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS	           		
         //=========Create Project Randomly===================================
         String Project_Name = "Projet_"+Generate_Random_Number.generateRandomValue();
         folderPage=projectDashboardPage.createProject(Project_Name);         		
         //=========Create Folder Randomly===================================
         String Foldername = "Folder_"+Generate_Random_Number.generateRandomValue();
         folderPage.New_Folder_Create(Foldername); 	
         folderPage.Select_Folder(Foldername);         		
         //================File Upload without Index=====================
         viewerScreenPage = new ViewerScreenPage(driver).get();
         String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
         String PhotoPath = PropertyReader.getProperty("Upload_PhotoFile");
         String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
         int FileCount = Integer.parseInt(CountOfFilesInFolder);
         folderPage.Upload_WithoutIndex(FolderPath, FileCount);     		
         viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
        
         //============RFI Creation ON first File===========================
         String parentHandle = driver.getWindowHandle();
		 viewerScreenPage.Image1();
		 viewerScreenPage.PhotoAnnotation(parentHandle);
		 viewerScreenPage.UploadPhoto(PhotoPath, FileCount);		 
		 viewerScreenPage.untitledphotovalidation();
		 driver.close();
		 driver.switchTo().window(parentHandle);
		 viewerScreenPage.Logout();	 
		 
		 // Log.assertThat( viewerScreenPage.Logout(), "Logout working Successfully","Logout not working Successfully", driver);

          	       			
          		
          	}
              catch(Exception e)
              {
              	e.getCause();
              	Log.exception(e, driver);
              }
          	finally
          	{
          		Log.endTestCase();
          		driver.close();
          	}
       	
       }  
      	  
      	   
     
@Test(priority = 12, enabled = true, description = "Delete 1st revision file from web where RFI is exist.Take the update change set in the device for deleted file.Verify the existance of RFI in the 2nd revision file.")
public void Delete1strevisionfilefromweb() throws Throwable
     {         	 
     try
     {
         Log.testCaseInfo("TC_01 (RFI Delete Related Testcases):Delete 1st revision file from web where RFI is exist.Take the update change set in the device for deleted file.Verify the existance of RFI in the 2nd revision file.");
         String uName = PropertyReader.getProperty("Username1");
         String pWord = PropertyReader.getProperty("Password1");
         //==============Login with valid UserID/PassWord===========================
         projectsLoginPage = new ProjectsLoginPage(driver).get();  
         projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS	           		
         //=========Create Project Randomly===================================
         String Project_Name = "Projet_"+Generate_Random_Number.generateRandomValue();
         folderPage=projectDashboardPage.createProject(Project_Name);         		
         //=========Create Folder Randomly===================================
         String Foldername = "Folder_"+Generate_Random_Number.generateRandomValue();
         folderPage.New_Folder_Create(Foldername); 	
         folderPage.Select_Folder(Foldername);         		
         //================File Upload without Index=====================
         viewerScreenPage = new ViewerScreenPage(driver).get();
         String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
         String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
         int FileCount = Integer.parseInt(CountOfFilesInFolder);
         folderPage.Upload_WithoutIndex(FolderPath, FileCount);     		
         viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
         folderPage.UploadFiles_WithoutIndexMycomputer(FolderPath, FileCount);
         viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
         viewerScreenPage = new ViewerScreenPage(driver).get();
         //viewerScreenPage.simpleexecutipn();
         //============RFI Creation ON first File===========================
         String parentHandle = driver.getWindowHandle();
		 viewerScreenPage.Image1();
		 viewerScreenPage.RFI_TestDataCreation_ownerLevel(parentHandle);
		 viewerScreenPage.RFI_OwnerLevel_validation();
		 driver.close();
		 driver.switchTo().window(parentHandle);
		 /*viewerScreenPage.Image1();		
		 viewerScreenPage.DeleteFirstRevisionfile(parentHandle);		 
		 driver.switchTo().window(parentHandle);		
		 
		 //===========RFI Creation Of Second Revision File==================
		 
		  String parentHandle1 = driver.getWindowHandle();
     	  viewerScreenPage.Image1();
     	  viewerScreenPage.RFI_RevisionFile(parentHandle1);
     	  viewerScreenPage.RFI_OwnerLevel_validation();
     	  viewerScreenPage.StatusValidation();     	 
     	  driver.close();
     	  SkySiteUtils.waitTill(5000);
     	  driver.switchTo().window(parentHandle1);*/
     	  
     	 /*String parentHandle2 = driver.getWindowHandle();
		 viewerScreenPage.Image1();		
		 viewerScreenPage.DeleteFirstRevisionfile(parentHandle);		 
		 driver.switchTo().window(parentHandle2);*/
     	  viewerScreenPage.Logout();	 
		 
		 // Log.assertThat( viewerScreenPage.Logout(), "Logout working Successfully","Logout not working Successfully", driver);

          	       			
          		
          	}
              catch(Exception e)
              {
              	e.getCause();
              	Log.exception(e, driver);
              }
          	finally
          	{
          		Log.endTestCase();
          		driver.close();
          	}
       	
           }  
      	  
     


}
