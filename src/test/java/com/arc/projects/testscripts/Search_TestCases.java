package com.arc.projects.testscripts;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.Gallery_Photo_Page;
import com.arc.projects.pages.ProjectAndFolder_Level_Search;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.Search_Testcases;
import com.arc.projects.pages.SubmitalPage;
import com.arc.projects.pages.ViewerScreenPage;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class Search_TestCases {
	 
	public static WebDriver driver;
	ProjectsLoginPage projectsLoginPage;
	ProjectDashboardPage projectDashboardPage;
	ViewerScreenPage viewerScreenPage;
	FolderPage folderPage;
	ProjectAndFolder_Level_Search projectAndFolder_Level_Search;
	Search_Testcases search_Testcases;
	Gallery_Photo_Page gallery_Photo_Page;
	SubmitalPage submitalPage;

	@Parameters("browser")
	@BeforeMethod
	public WebDriver beforeTest(String browser) {

		if (browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			// System.setProperty("webdriver.gecko.driver",
			// dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}

		else if (browser.equalsIgnoreCase("chrome")) {
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory", "C:" + File.separator + "Users" + File.separator
					+ System.getProperty("user.name") + File.separator + "Downloads");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			options.setExperimentalOption("prefs", prefs);
			driver = new ChromeDriver(options);
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));

		}

		else if (browser.equalsIgnoreCase("safari")) {
			System.setProperty("webdriver.safari.noinstall", "true"); 
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}

	 /*
	 * TC_002:-----Verify user is able to do project search (Module )
	 */

	@Test(priority = 0, enabled = true, description = "Verify user is able to do project search (Module Search)")
	public void ProjectName_ModuleSearch() throws Throwable {

		try {
			Log.testCaseInfo("TC_002:-Verify user is able to do project search (Module Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with valid UserID/PassWord=============
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// =========Create Proj Randomly===================================
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Searchscenario(Project_Name);
			search_Testcases.Searchprojectvalidation(Project_Name);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.close();
		}

	}

	 /*
	 * TC_003:-----Verify user is able to do project search (Filter Search By
	 * ProjectName)
	 */

	@Test(priority = 1, enabled = true, description = "Verify user is able to do filter Search -Project Name")
	public void FilterSearch_ProjectName() throws Throwable {

		try {
			Log.testCaseInfo("TC_003 :-Verify user is able to do filter Search -Project Name");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");	
			String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
			String Project_Number = Generate_Random_Number.generateRandomValue();
			String Description = PropertyReader.getProperty("ProjectDescription");
			String Country = PropertyReader.getProperty("country");
			String City = PropertyReader.getProperty("city");
			String State = PropertyReader.getProperty("state");
		    //===========Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);						
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.CreateProject(Project_Name, Project_Number, Description, Country, City, State);						
			String Flag="Search_ProjectName";
			search_Testcases.AdvanceSearch_Project(Project_Name, Flag);
				
			
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.close();
		}

	}
	

	/*
	 * TC_004:-----Verify user is able to Search by Folder name (Module search )
	 */

	@Test(priority = 2, enabled = true, description = "Verify user is able to do project search (Filter Search By Project Name)")
	public void FolderName_ModuleSearch() throws Throwable {

		try {
			Log.testCaseInfo("TC_004:-Verify user is able to do project search (Filter Search By Project Name)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Foldername = "Folder_" + Generate_Random_Number.generateRandomValue();
			// ==============Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// =========Create ProjectRandomly===================================
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			folderPage = new FolderPage(driver).get();
			folderPage.New_Folder_Create(Foldername);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Searchscenario(Foldername);
			search_Testcases.SearchFoldervalidation(Foldername);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.close();
		}

	}

	/*
	 * TC_005:---Verify user is able to do File search (Module )
	 */

	@Test(priority = 3, enabled = true, description = "Verify user is able to do  File search (Module )")
	public void FileName_ModuleSearch() throws Throwable {

		try {
			Log.testCaseInfo("TC_005:-Verify user is able to do Folder search (Filter Search By Folder Name)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Foldername = "Folder_" + Generate_Random_Number.generateRandomValue();
			String Filename = PropertyReader.getProperty("FileName");
			// ==============Login with valid UserID/PassWord======
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			//=========Create Project Randomly======================
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			folderPage = new FolderPage(driver).get();
			folderPage.New_Folder_Create(Foldername);
			folderPage.Select_Folder(Foldername);
			//============File Upload without Index================
			String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
			String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
			int FileCount = Integer.parseInt(CountOfFilesInFolder);
			folderPage.Upload_WithoutIndex(FolderPath, FileCount);
			viewerScreenPage = new ViewerScreenPage(driver).get();
			viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Searchscenario(Filename);
			search_Testcases.SearchFilevalidation(Filename);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.close();
		}

	}

	/*
	 * TC_006:---Verify user is able to do Album (Module search)
	 */

	@Test(priority = 4, enabled = true, description = "Verify user is able to do Album (Module")
	public void Album_ModuleSearch() throws Throwable {

		try {
			Log.testCaseInfo("TC_006:-Verify user is able to do Album (Module search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Foldername = "Folder_" + Generate_Random_Number.generateRandomValue();
			String Albumname = Generate_Random_Number.AlbumName();
			String Filename = PropertyReader.getProperty("FileName");
			//==============Login with valid UserID/PassWord========
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			//=========Create Projec Randomly========================
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			folderPage = new FolderPage(driver).get();
			folderPage.Select_Gallery_Folder();
			gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Verify_AddAlbum(Albumname);
			search_Testcases.Searchscenario(Albumname);
			search_Testcases.Searchfolderphotovalidation(Albumname);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.close();
		}

	}
	
	

	 /*
	 * TC_007:---Verify user is able to do Photo  Search (Module)
	 */

	@Test(priority = 5, enabled = true, description = "Verify user is able to do Photo  (Module search")
	public void photofilterSearch() throws Throwable {

		try {
			Log.testCaseInfo("TC_007:-Verify user is able to do Photo(Module search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String photo = PropertyReader.getProperty("photoName");
			String photolocation = PropertyReader.getProperty("Upload_PhotoFile");
			String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
			int FileCount = Integer.parseInt(CountOfFilesInFolder);			
			String Foldername = "Folder_" + Generate_Random_Number.generateRandomValue();
			String Filename = PropertyReader.getProperty("FileName");
			String Albumname = Generate_Random_Number.AlbumName();
		    // ==============Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);			
			// ================File Upload without Index Randomly===================================						
			folderPage = new FolderPage(driver).get();
			folderPage.Select_Gallery_Folder();			
			gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Verify_AddAlbum(Albumname);
			search_Testcases.Searchscenario(Albumname);
			search_Testcases.Searchscenario(Albumname);			
			search_Testcases.SelectGalleryFolder(Albumname);
			search_Testcases.UploadPhotos_InGallery(photolocation, FileCount);
			search_Testcases.Searchscenario(photo);
			//search_Testcases.Searchfolderphotovalidation(Albumname);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.close();
		}

	}
	
	
	
	 /*
	 * TC_008:- Verify user is able to do  Photos search (Filter)
	 */	
	
	
	@Test(priority = 6, enabled = true, description = "Verify user is able to do  Photos search (Filter)")
	public void PhotoSearch_Module() throws Throwable {

		try {
			Log.testCaseInfo("TC_008:-Verify user is able to do Photo Search (Filter search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String photo = PropertyReader.getProperty("photoName");
			String photolocation = PropertyReader.getProperty("Upload_PhotoFile");
			String Building = PropertyReader.getProperty("Building");
			String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
			int FileCount = Integer.parseInt(CountOfFilesInFolder);			
			String Foldername = "Folder_" + Generate_Random_Number.generateRandomValue();
			String Filename = PropertyReader.getProperty("FileName");
			String Albumname = Generate_Random_Number.AlbumName();
		    // ==============Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);			
			// ================File Upload without Index Randomly===================================						
			folderPage = new FolderPage(driver).get();
			folderPage.Select_Gallery_Folder();			
			gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Verify_AddAlbum(Albumname);
			search_Testcases.Searchscenario(Albumname);
			search_Testcases.Searchscenario(Albumname);			
			search_Testcases.SelectGalleryFolder(Albumname);
			search_Testcases.UploadPhotos_InGallery(photolocation, FileCount);
			search_Testcases.AdvanceSearch_building(Building);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.close();
		}

	}
	
		
	     /*
		 * TC_009:- Verify user is able to do Punch,(Module)
		 */	
		
		@Test(priority = 7, enabled = true, description = "Verify user is able to do Punch,(Module search)")
		public void PunchSearch_Module() throws Throwable {

			try {
				Log.testCaseInfo("TC_009 :-Verify user is able to do Punch,(Module Search)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");				
			    //===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);			
				//=========File Upload without Index Randomly===================================						
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();		         		
         		projectAndFolder_Level_Search.ProjectManagement_Punch();	
         		viewerScreenPage = new ViewerScreenPage(driver).get();
    			viewerScreenPage.PunchCreation(); 
    			search_Testcases = new Search_Testcases(driver).get();
    			search_Testcases.Searchscenario_punch();
				

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
	
		         /*
				 * TC_010:- Verify user is able to do Punch,(Filter Search)
				 */	
				
				
				@Test(priority = 8, enabled = true , description = "Verify user is able to do Punch,(Filter Search)")
				public void PunchSearch_Filter() throws Throwable {

					try {
						Log.testCaseInfo("TC_010 :-Verify user is able to do Punch,(Filter Search)");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");				
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						String Project_Name = PropertyReader.getProperty("StaticProject");
						projectDashboardPage.ValidateSelectProject(Project_Name);			
						//=========File Upload without Index Randomly===================================						
						projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();		         		
		         		projectAndFolder_Level_Search.ProjectManagement_Punch();	
		         		viewerScreenPage = new ViewerScreenPage(driver).get();
		    			viewerScreenPage.PunchCreation(); 
		    			search_Testcases = new Search_Testcases(driver).get();
		    			search_Testcases.AdvanceSearch_Description();
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
			
			    /*
				* TC_011:- Verify user is able to do RFI,(MODULE Search)
				 */	
				
				
				@Test(priority = 9, enabled = true , description = "Verify user is able to do RFI(Module Search)")
				public void RFISearch_Module() throws Throwable {

					try {
						Log.testCaseInfo("TC_011 :-Verify user is able to do RFI(Module Search)");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");				
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						String Project_Name = PropertyReader.getProperty("StaticProject");
						projectDashboardPage.ValidateSelectProject(Project_Name);			
					    projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
		         		projectAndFolder_Level_Search.ProjectManagement();        		
		         	    projectAndFolder_Level_Search.RFICREATION_New() ;
		         	    search_Testcases = new Search_Testcases(driver).get();
		         	    search_Testcases.Searchscenario_RFI();
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
			
				 /*
				  * TC_012:- Verify user is able to Search RFI with RFI Number (Filter Search)
				 */	
				
				
				@Test(priority = 10, enabled = true , description = "Verify user is able to Search RFI with RFI Number (Filter Search)")
				public void RFISearch_Filter() throws Throwable {

					try {
						Log.testCaseInfo("TC_012 :- Verify user is able to Search RFI with RFI Number(Filter Search)");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");				
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						String Project_Name = PropertyReader.getProperty("StaticProject");
						projectDashboardPage.ValidateSelectProject(Project_Name);			
					    projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
		         		projectAndFolder_Level_Search.ProjectManagement();        		
		         	    projectAndFolder_Level_Search.RFICREATION_New() ;
		         	    search_Testcases = new Search_Testcases(driver).get();
		         	    search_Testcases.RFI_AdvanceSearchWithRFINumber();
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
				  /*
				  * TC_013:- Verify user is able to Search RFI with RFI Due Date (Filter Search)
				  */	
				
				
				@Test(priority = 11, enabled = true, description = "Verify user is able to Search RFI with RFI Due Date (Filter Search)")
				public void RFISearchFilter() throws Throwable {

					try {
						Log.testCaseInfo("TC_013 :-Verify user is able to Search RFI with RFI Due Date (Filter Search)");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");				
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						String Project_Name = PropertyReader.getProperty("StaticProject");
						projectDashboardPage.ValidateSelectProject(Project_Name);			
					    projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
		         		projectAndFolder_Level_Search.ProjectManagement(); 
		         		//projectAndFolder_Level_Search.RFICREATION_New() ;
		         	    search_Testcases = new Search_Testcases(driver).get();
		         	    search_Testcases.RFI_AdvanceSearchWithRFIDueDate();
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
				 /*
				  * TC_014:- Verify user is able to Search Submittal (Module Search)
				  */	
				
				
				@Test(priority = 12, enabled = true, description = "Verify user is able to Search Submittal (Module Search)")
				public void SubmitalSearchModule() throws Throwable {

					try {
						Log.testCaseInfo("TC_014 :-Verify user is able to Search Submittal (Module Search)");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");	
						String Submittal_Number = "SubId_"+Generate_Random_Number.generateRandomValue();
			         	String Submittal_Name = "SubName_"+Generate_Random_Number.generateRandomValue();			         	
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						String Project_Name = PropertyReader.getProperty("StaticProject");
						projectDashboardPage.ValidateSelectProject(Project_Name);			
					    projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
		         		projectAndFolder_Level_Search.ProjectManagement_Submittal();	         		
		         		search_Testcases = new Search_Testcases(driver).get();
		         		search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);
		         		search_Testcases.Searchscenario(Submittal_Number);
		         		//search_Testcases.SubmittalNumber_Validation(Submittal_Number);
		         		
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
				
				 /*
				  * TC_015:- Verify user is able to filter Search Submittal number (Filter Search)
				  */	
				
				
				@Test(priority = 13, enabled =true, description = "Verify user is able to filter Search Submittal number (Filter Search)")
				public void SubmitalfilterSearch_Number() throws Throwable {

					try {
						Log.testCaseInfo("TC_015 :-Verify user is able to filter Search Submittal number (Filter Search)");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");	
						String Submittal_Number = Generate_Random_Number.generateRandomValue();
			         	String Submittal_Name = "SubName_"+Generate_Random_Number.generateRandomValue();			         	
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						String Project_Name = PropertyReader.getProperty("StaticProject");
						projectDashboardPage.ValidateSelectProject(Project_Name);			
					    projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
		         		projectAndFolder_Level_Search.ProjectManagement_Submittal(); 
		         		search_Testcases = new Search_Testcases(driver).get();
		         		search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);	
		         		projectAndFolder_Level_Search.ProjectManagement_Submittal(); 
		         		search_Testcases = new Search_Testcases(driver).get();						
						String Flag="Search_SubmittalNumber";
						search_Testcases.AdvanceSearch_Submittal(Submittal_Number, Flag);
							
		         		
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
				 /*
				  * TC_016:- Verify user is able to filter Search Submittal Name (Filter Search)
				  */
				
				@Test(priority = 14, enabled =true, description = "Verify user is able to filter Search Submittal Name (Filter Search)")
				public void SubmitalfilterSearch_Name() throws Throwable {

					try {
						Log.testCaseInfo("TC_016 :-Verify user is able to filter Search Submittal Name (Filter Search)");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");	
						String Submittal_Number = "SubId_"+Generate_Random_Number.generateRandomValue();
			         	String Submittal_Name = "SubName_"+Generate_Random_Number.generateRandomValue();
			         	
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						String Project_Name = PropertyReader.getProperty("StaticProject");
						projectDashboardPage.ValidateSelectProject(Project_Name);			
					    projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
		         		
		         		projectAndFolder_Level_Search.ProjectManagement_Submittal(); 
		         		search_Testcases = new Search_Testcases(driver).get();
		         		search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);
		         		projectAndFolder_Level_Search.ProjectManagement_Submittal(); 
		         		search_Testcases = new Search_Testcases(driver).get();						
						String Flag="Search_SubmittalName";
						search_Testcases.AdvanceSearch_Submittal(Submittal_Name, Flag);
						
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
				
				 /*
				  * TC_017:- Verify user is able to filter Search Submittal Type (Filter Search)
				  */
				
				@Test(priority = 15, enabled = true, description = "Verify user is able to filter Search Submittal Type (Filter Search)")
				public void SubmitalfilterSearch_Type() throws Throwable {

					try {
						Log.testCaseInfo("TC_017 :-Verify user is able to filter Search Submittal Type (Filter Search)");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");	
						String Submittal_Number = "SubId_"+Generate_Random_Number.generateRandomValue();
			         	String Submittal_Name = "SubName_"+Generate_Random_Number.generateRandomValue();			         	
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						String Project_Name = PropertyReader.getProperty("StaticProject");
						projectDashboardPage.ValidateSelectProject(Project_Name);			
					    projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
					    projectAndFolder_Level_Search.ProjectManagement_Submittal(); 
		         		search_Testcases = new Search_Testcases(driver).get();
		         		search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);
		         		String CommonValue = PropertyReader.getProperty("SubmittalType");	
		         		String Flag="Search_SubmittalType";		         
		         		search_Testcases.AdvanceSearch_Submittal(CommonValue, Flag);
		         		
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
				/*
				  * TC_018:- Verify user is able to filter Search Submittal Status (Filter Search)
				  */
				
	@Test(priority = 16, enabled = true, description = "Verify user is able to filter Search Submittal Status (Filter Search)")
	public void SubmitalfilterSearch_Status() throws Throwable {

		try {
			Log.testCaseInfo("TC_018 :-Verify user is able to filter Search Submittal Status (Filter Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Submittal_Number = "SubId_" + Generate_Random_Number.generateRandomValue();
			String Submittal_Name = "SubName_" + Generate_Random_Number.generateRandomValue();

			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();

			projectAndFolder_Level_Search.ProjectManagement_Submittal();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);
			String CommonValue = PropertyReader.getProperty("Status");
			String Flag="Search_Status";		         
     		search_Testcases.AdvanceSearch_Submittal(CommonValue, Flag);
     		
			
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.close();
		}

	}
	
	
	  /*
	  * TC_019:- Verify user is able to filter Search Submittal Due Date (Filter Search)
	  */
				
				@Test(priority = 17, enabled = true , description = "Verify user is able to filter Search Submittal Due Date (Filter Search)")
				public void SubmitalfilterSearch_DueDate() throws Throwable {

					try {
						Log.testCaseInfo("TC_019 :-Verify user is able to filter Search Submittal Due Date (Filter Search)");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");	
						String Submittal_Number = "SubId_"+Generate_Random_Number.generateRandomValue();
			         	String Submittal_Name = "SubName_"+Generate_Random_Number.generateRandomValue();
			         	
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						String Project_Name = PropertyReader.getProperty("StaticProject");
						projectDashboardPage.ValidateSelectProject(Project_Name);			
					    projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
		         		projectAndFolder_Level_Search.ProjectManagement(); 
		         		
		         		submitalPage = new SubmitalPage(driver).get();
		         		submitalPage.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);
		         		//projectAndFolder_Level_Search.RFICREATION_New() ;
		         	    search_Testcases = new Search_Testcases(driver).get();
		         	    search_Testcases.RFI_AdvanceSearchWithRFIDueDate();
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
				
				
				//===================Contacts Related Testcases=========================				
				
				/*TC_020.Verify user is able to do filter Search- First Name*/
				
				@Test(priority = 18, enabled = true, description = "Verify user is able to do filter Serarch First Name (Filter Search)")
				public void ContactSearch_FirstName() throws Throwable {

					try {
						Log.testCaseInfo("TC_020:-Verify user is able to do filter Serarch First Name (Filter Search)");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");						
					    //===========Login with valid UserID/PassWord==========
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						search_Testcases = new Search_Testcases(driver).get();
						String Contact_FirstName = PropertyReader.getProperty("Contact_FirstName");
						String Flag="Search_FirstName";
						search_Testcases.AdvanceSearch_Contact(Contact_FirstName, Flag);
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
				
                /*TC_021.Verify user is able to do filter Search- Last Name*/
				
				@Test(priority = 19, enabled =true, description = "Verify user is able to do filter Search Last Name (Filter Search)")
				public void ContactSearch_LastName() throws Throwable {

					try {
						Log.testCaseInfo("TC_021 :-Verify user is able to do filter Search Last Name ");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");	
						
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						search_Testcases = new Search_Testcases(driver).get();
						String Contact_LastName = PropertyReader.getProperty("Contact_LastName");
						String Flag="Search_LastName";
						search_Testcases.AdvanceSearch_Contact(Contact_LastName, Flag);
						
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
               /*TC_022.Verify user is able to do filter Search- Email*/
				
				@Test(priority = 20, enabled = true, description = "Verify user is able to do filter Search Last Name (Filter Search)")
				public void ContactSearch_Email() throws Throwable {

					try {
						Log.testCaseInfo("TC_022 :-Verify user is able to do filter Search Email");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");	
						
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						search_Testcases = new Search_Testcases(driver).get();
						String Contact_Email = PropertyReader.getProperty("Contact_Email");
						String Flag="Search_EMAIL";
						search_Testcases.AdvanceSearch_Contact(Contact_Email, Flag);
						
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
				
               /*23.Verify user is able to do filter Search- Phone*/
				
				@Test(priority = 21, enabled =true, description = "Verify user is able to do filter Search PHONE ")
				public void ContactSearch_Phone() throws Throwable {

					try {
						Log.testCaseInfo("TC_023 :-Verify user is able to do filter Search Phone");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");	
						
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						search_Testcases = new Search_Testcases(driver).get();
						String Contact_phone = PropertyReader.getProperty("Contact_Phone");
						String Flag="Search_PHONE";
						search_Testcases.AdvanceSearch_Contact(Contact_phone, Flag);
									
					    
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
				
                /*TC_024.Verify user is able to do filter Search- City*/
				
				@Test(priority = 22, enabled = true, description = "Verify user is able to do filter Search CITY ")
				public void ContactSearch_City() throws Throwable {

					try {
						Log.testCaseInfo("TC_024 :-Verify user is able to do filter Search City");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");	
						
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						search_Testcases = new Search_Testcases(driver).get();
						String Contact_CITY = PropertyReader.getProperty("Contact_City");
						String Flag="Search_CITY";
						search_Testcases.AdvanceSearch_Contact(Contact_CITY, Flag);
										
					    
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
				
                /*TC_025.Verify user is able to do filter Search- Address*/
				
				@Test(priority = 23, enabled = true, description = "Verify user is able to do filter Search Address ")
				public void ContactSearch_Address() throws Throwable {

					try {
						Log.testCaseInfo("TC_025 :-Verify user is able to do filter Search Address");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");	
						
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						search_Testcases = new Search_Testcases(driver).get();
						String Contact_Address = PropertyReader.getProperty("Contact_Address");
						String Flag="Search_Address";
						search_Testcases.AdvanceSearch_Contact(Contact_Address, Flag);
							
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
				
				
               /*TC_026.Verify user is able to punch filter Search- Description*/
				
				@Test(priority = 24, enabled = true, description = "Verify user is able to do filter Search Punch - Description")
				public void FilterSearch_Punch_Description() throws Throwable {

					try {
						Log.testCaseInfo("TC_026 :-Verify user is able to do filter Search Punch - Description");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");							
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						String Project_Name = PropertyReader.getProperty("StaticProject");
						projectDashboardPage.ValidateSelectProject(Project_Name);			
					    projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
		         		projectAndFolder_Level_Search.ProjectManagement_Punch(); 
		         		viewerScreenPage = new ViewerScreenPage(driver).get();
		         		viewerScreenPage.PunchCreation();         		
						search_Testcases = new Search_Testcases(driver).get();
						String Description_sub = PropertyReader.getProperty("Punch_Subject");
						String Flag="Search_Description";
						search_Testcases.AdvanceSearch_Punch(Description_sub, Flag);
							
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
				
                /*TC_027.Verify user is able to punch filter Search- creator*/
				
				@Test(priority = 25, enabled = true, description = "Verify user is able to do filter Search Punch - Creator")
				public void FilterSearch_Punch_Creator() throws Throwable {

					try {
						Log.testCaseInfo("TC_027 :-Verify user is able to do filter Search Punch - Creator");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");	
						
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						
						String Project_Name = PropertyReader.getProperty("StaticProject");
						projectDashboardPage.ValidateSelectProject(Project_Name);			
					    projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
		         		projectAndFolder_Level_Search.ProjectManagement_Punch(); 
		         		
						viewerScreenPage = new ViewerScreenPage(driver).get();
		         		viewerScreenPage.PunchCreation(); 
						search_Testcases = new Search_Testcases(driver).get();
						String creator = PropertyReader.getProperty("Creator");
						String Flag="Search_Creator";
						search_Testcases.AdvanceSearch_Punch(creator, Flag);
							
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
               /*TC_028.Verify user is able to punch filter Search- Assignee*/
				
				@Test(priority = 26, enabled = true, description = "Verify user is able to do filter Search Punch - Assignee")
				public void FilterSearch_Punch_Assignee() throws Throwable {

					try {
						Log.testCaseInfo("TC_028 :-Verify user is able to do filter Search Punch - Assignee");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");						
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						//======================================================================
						String Project_Name = PropertyReader.getProperty("StaticProject");
						projectDashboardPage.ValidateSelectProject(Project_Name);			
					    projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
		         		projectAndFolder_Level_Search.ProjectManagement_Punch(); 
		         		
						viewerScreenPage = new ViewerScreenPage(driver).get();
		         		viewerScreenPage.PunchCreation(); 
						search_Testcases = new Search_Testcases(driver).get();
						String Assigne = PropertyReader.getProperty("SelectUserAssignTo_Email");
						String Flag="Search_Assignee";
						search_Testcases.AdvanceSearch_Punch(Assigne, Flag);
							
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
                /*TC_029.Verify user is able to punch filter Search- Status*/
				
				@Test(priority = 27, enabled = true, description = "Verify user is able to do filter Search Punch - Status")
				public void FilterSearch_Punch_Status() throws Throwable {

					try {
						Log.testCaseInfo("TC_029 :-Verify user is able to do filter Search Punch - Status");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");						
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
						
						String Project_Name = PropertyReader.getProperty("StaticProject");
						projectDashboardPage.ValidateSelectProject(Project_Name);			
					    projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
		         		projectAndFolder_Level_Search.ProjectManagement_Punch();
		         		viewerScreenPage = new ViewerScreenPage(driver).get();
		         		viewerScreenPage.PunchCreation(); 
						search_Testcases = new Search_Testcases(driver).get();
						String status = PropertyReader.getProperty("Status");
						String Flag="Search_Status";
						search_Testcases.AdvanceSearch_Punch(status, Flag);
							
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
				//==============project Related search========================================
				
				      
				
                   
				
                /*TC_030.Verify user is able do Filter Search- Project Number*/
				
				@Test(priority = 28, enabled = true, description = "Verify user is able to do filter Search -Project Number")
				public void FilterSearch_ProjectNumber() throws Throwable {

					try {
						Log.testCaseInfo("TC_030 :-Verify user is able to do filter Search -Project Number");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");	
						String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
						String Project_Number =Generate_Random_Number.generateRandomValue();
						String Description = PropertyReader.getProperty("ProjectDescription");
						String Country = PropertyReader.getProperty("country");
						String City = PropertyReader.getProperty("city");
						String State = PropertyReader.getProperty("state");
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);						
						search_Testcases = new Search_Testcases(driver).get();
						search_Testcases.CreateProject(Project_Name, Project_Number, Description, Country, City, State);						
						String Flag="Search_ProjectNumber";
						search_Testcases.AdvanceSearch_Project(Project_Number, Flag);
							
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}
				
                /*TC_031.Verify user is able do Filter Search- Project Description*/
				
				@Test(priority = 29, enabled = true , description = "Verify user is able to do filter Search -Description")
				public void FilterSearch_Description() throws Throwable {

					try {
						Log.testCaseInfo("TC_031 :-Verify user is able to do filter Search -Description");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");
						String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
						String Project_Number =Generate_Random_Number.generateRandomValue();
						String Description = Generate_Random_Number.generateRandomValue()+"desc";
						String Country = PropertyReader.getProperty("country");
						String City = PropertyReader.getProperty("city");
						String State = PropertyReader.getProperty("state");
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);						
						search_Testcases = new Search_Testcases(driver).get();
						search_Testcases.CreateProject(Project_Name, Project_Number, Description, Country, City, State);						
						String Flag="Search_ProjectDescription";
						search_Testcases.AdvanceSearch_Project(Description, Flag);
							
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}	
				
				
                /*TC_032.Verify user is able do Filter Search- project City*/
				
				@Test(priority = 30, enabled = true, description = "Verify user is able to do filter Search -CITY")
				public void FilterSearch_CITY() throws Throwable {

					try {
						Log.testCaseInfo("TC_032 :-Verify user is able to do filter Search -CITY");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");
						String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
						String Project_Number =Generate_Random_Number.generateRandomValue();
						String Description = Generate_Random_Number.generateRandomValue()+"desc";
						String Country = PropertyReader.getProperty("country");
						String City = PropertyReader.getProperty("city");
						String State = PropertyReader.getProperty("state");
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);						
						search_Testcases = new Search_Testcases(driver).get();
						search_Testcases.CreateProject(Project_Name, Project_Number, Description, Country, City, State);						
						String Flag="Search_City";
						search_Testcases.AdvanceSearch_Project(City, Flag);
							
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}	
				
                    /*TC_033.Verify user is able do Filter Search-State*/
				
				@Test(priority = 31, enabled = true, description = "Verify user is able to do filter Search -State")
				public void FilterSearch_State() throws Throwable {

					try {
						Log.testCaseInfo("TC_033 :-Verify user is able to do filter Search -State");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");						
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);						
						
						search_Testcases = new Search_Testcases(driver).get();
						String status = PropertyReader.getProperty("Status");
						String Flag="Search_Status";
						search_Testcases.AdvanceSearch_Punch(status, Flag);
							
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}	
				
				
				
                 /*TC_034.Verify user is able do Filter Search-Country*/
				
				@Test(priority = 32, enabled = true, description = "Verify user is able to do filter Search -Country")
				public void FilterSearch_Country() throws Throwable {

					try {
						Log.testCaseInfo("TC_034 :-Verify user is able to do filter Search -Country");
						String uName = PropertyReader.getProperty("Username1");
						String pWord = PropertyReader.getProperty("Password1");						
					    //===========Login with valid UserID/PassWord===========================
						projectsLoginPage = new ProjectsLoginPage(driver).get();
						projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);						
						String Project_Name = PropertyReader.getProperty("StaticProject");
						projectDashboardPage.ValidateSelectProject(Project_Name);			
					    projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
		         		projectAndFolder_Level_Search.ProjectManagement_Punch();
		         		viewerScreenPage = new ViewerScreenPage(driver).get();
		         		viewerScreenPage.PunchCreation(); 
						search_Testcases = new Search_Testcases(driver).get();
						String status = PropertyReader.getProperty("Status");
						String Flag="Search_Status";
						search_Testcases.AdvanceSearch_Punch(status, Flag);
							
		    			
					} catch (Exception e) {
						e.getCause();
						Log.exception(e, driver);
					} finally {
						Log.endTestCase();
						driver.close();
					}

				}	
				
				
				
				
				
				
				
				
				
				
				
				
				
				


}
