package com.arc.projects.utils;

import java.awt.AWTException;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.openqa.selenium.OutputType;
import com.arcautoframe.utils.Log;
import com.google.common.base.Function;
//import com.mongodb.MapReduceCommand.OutputType;

public class CommonMethod {

	
	static WebDriver driver ;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Alert alert = null;

	/*public static void Fluentwait(WebElement element, int timelimit, int interval) throws IOException {

		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)

				.withTimeout(timelimit, TimeUnit.SECONDS)

				.pollingEvery(interval, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

		WebElement foo = wait.until(new Function<WebDriver, WebElement>() {

			public WebElement apply(WebDriver driver) {

				return element;

			}
		});

	}*/
	
	
	
	public static boolean fluentWait(WebElement element, int timelimit, int interval){
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
			    .withTimeout(timelimit, TimeUnit.SECONDS)
			    .pollingEvery(interval, TimeUnit.SECONDS)
			    .ignoring(NoSuchElementException.class);

			WebElement foo = wait.until(new Function<WebDriver, WebElement>() 
			{
			  public WebElement apply(WebDriver driver) {
			  return element;
			}
			});
		return true;
	    
	};
	
	
	
	

	public static boolean ScreenShotforParticulararea(WebElement element, String FileLocation) throws IOException {
		boolean result = false;
		Log.message("screenshot Method ");
		// FOR PARTICULAR AREA SCREENSHOT
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		BufferedImage fullImg = ImageIO.read(screenshot);
		org.openqa.selenium.Point point = element.getLocation();
		// Get width and height of the element
		int eleWidth = element.getSize().getWidth();
		int eleHeight = element.getSize().getHeight();
		Log.message("Width of the screenshot " + eleWidth);
		Log.message("Width of the screenshot " + eleHeight);
		BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth, eleHeight);
		ImageIO.write(eleScreenshot, "png", screenshot);
		Log.message("bufferImage " + eleScreenshot);
		File screenshotLocation = new File(FileLocation + ".jpg");
		FileUtils.copyFile(screenshot, screenshotLocation);
		return result;

	}

	public static boolean compareTwoImages(String File1, String File2) {
		Boolean isTrue = false;

		Log.message("enter into image1 " + File1);
		Log.message("enter into image1 " + File2);
		File f1 = new File(File1);
		File f2 = new File(File2);
		try {
			Log.message("enter into image " + f1);
			// Image imgOne = ImageIO.read(f1);
			// Image imgTwo = ImageIO.read(f2);
			BufferedImage bufImgOne = ImageIO.read(f1);
			BufferedImage bufImgTwo = ImageIO.read(f2);
			int imgOneHt = bufImgOne.getHeight();
			int imgTwoHt = bufImgTwo.getHeight();
			int imgOneWt = bufImgOne.getWidth();
			int imgTwoWt = bufImgTwo.getWidth();
			if (imgOneHt != imgTwoHt || (imgOneWt != imgTwoWt)) {
				System.out.println(" size are not equal ");
				isTrue = false;
			}
			for (int x = 0; x < imgOneHt; x++) {
				for (int y = 0; y < imgOneWt; y++) {
					if (bufImgOne.getRGB(x, y) != bufImgTwo.getRGB(x, y)) {
						System.out.println(" size are not equal ");
						isTrue = false;
						break;
					}
				}
			}
		} catch (IOException e) {

			e.printStackTrace();
		}
		return isTrue;
	}

	// Image Comparision Method to compare Image file

	public static boolean Imagecomparision(String File1, String File2) {

		boolean result = true;
		int file1_height, file1_width, file2_height, file2_width;
		try {
			File f1 = new File(File1);
			File f2 = new File(File2);
			BufferedImage b1 = ImageIO.read(f1);
			BufferedImage b2 = ImageIO.read(f2);
			file1_height = b1.getHeight();
			Log.message("file1_height is: " + file1_height);
			file1_width = b1.getWidth();
			Log.message("file1_width is: " + file1_width);
			file2_height = b2.getHeight();
			Log.message("file2_height is: " + file2_height);
			file2_width = b2.getWidth();
			Log.message("file2_width is: " + file2_width);
			int Total_Pass_Count = 0;
			int Total_Fail_Count = 0;

			if (file1_height == file2_height && file2_width == file1_width) {
				int i, j;

				for (i = 1; i <= file1_height; i++) {
					for (j = 1; j <= file1_width; j++) {
						if (b1.getRGB(i, j) == b2.getRGB(i, j)) {
							Total_Pass_Count = Total_Pass_Count + 1;

							break;
						} else {
							Total_Fail_Count = Total_Fail_Count + 1;
						}
					}
				}
			}
			Log.message("Total_Pass_Count is: " + Total_Pass_Count);
			Log.message("Total_Fail_Count is: " + Total_Fail_Count);
			if ((Total_Pass_Count == file1_height) && (Total_Fail_Count == 0)) {
				Log.message("Both Image Are same :  + Passed");
				return true;
			} else {
				Log.message("Both Image Are not same: + Failed");
				return false;
			}
		}

		catch (Exception e) {
			e.printStackTrace();

		}
		return result;
	}

	public static boolean JSClick(By locator) throws Throwable {
		boolean flag = false;
		try {
			WebElement element = driver.findElement(locator);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
			// driver.executeAsyncScript("arguments[0].click();", element);

			flag = true;

		}

		catch (Exception e) {

		}
		return flag;
	}

	public static boolean selectBySendkeys(WebElement element, String value) throws Throwable {

		boolean flag = false;
		try {

			element.sendKeys(value);
			// driver.findElement(locator).sendKeys(value);
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		}

	}

	public static boolean selectByValue(WebElement element, String value) {
		boolean flag = false;
		try {
			// WebElement element=(WebElement)
			// driver.findElements(state_INBOX2);
			Select se = new Select(element);
			se.selectByValue(value);
			Thread.sleep(500);
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		}
	}

	public void DragAndDrop(WebElement toDrag, WebElement toDrop) {
		try {
			Actions builder = new Actions(driver);
			builder.keyDown(Keys.CONTROL).click(toDrag).dragAndDrop(toDrag, toDrop).keyUp(Keys.CONTROL);

			Action selected = builder.build();
			selected.perform();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void waitForPageLoaded() throws HeadlessException, AWTException, IOException {
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
						.equals("complete");
			}
		};
		try {
			Thread.sleep(1000);
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(expectation);
		} catch (Throwable error) {
			// takeScreenShotOnError(error.toString());
			Assert.fail("Timeout waiting for Page Load Request to complete.");
		}
	}

	public static boolean isElementPresent(By by) throws Throwable {
		boolean flag = false;
		try {
			driver.findElement(by);
			flag = true;
			return true;
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}

	}

	public static boolean js_type(WebElement element, String Text) throws Throwable {
		boolean flag = true;
		try {

			((JavascriptExecutor) driver).executeScript("arguments[0].value='" + Text + "'", element);
			return true;
		} catch (Exception e) {
			flag = false;
			return false;
		}
	}

	/**
	 * Moves the mouse to the middle of the element. The element is scrolled
	 * into view and its location is calculated using getBoundingClientRect.
	 * 
	 * @param locator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * 
	 */

	public static boolean mouseover(WebElement element) throws Throwable {
		boolean flag = false;
		try {
			// WebElement mo = driver.findElement(locator);
			new Actions(driver).moveToElement(element).build().perform();
			flag = true;
			return true;
		} catch (Exception e) {
			Assert.assertTrue(flag, "MouseOver action is not perform on ");
			return false;
		}

	}

	// =================Function for Drag And
	// Drop===============================

	/**
	 * A convenience method that performs click-and-hold at the location of the
	 * source element, moves by a given offset, then releases the mouse.
	 * 
	 * @param source
	 *            : Element to emulate button down at.
	 * 
	 * @param xOffset
	 *            : Horizontal move offset.
	 * 
	 * @param yOffset
	 *            : Vertical move offset.
	 * 
	 */

	public static boolean draggable(WebElement source, int x, int y) throws Throwable {
		boolean flag = false;
		try {

			new Actions(driver).dragAndDropBy(source, x, y).build().perform();
			Thread.sleep(5000);
			flag = true;
			return true;

		} catch (Exception e) {

			return false;
		}
	}

	// ===========create Function for scrollBar=====================
	/**
	 * To slide an object to some distance
	 * 
	 * @param slider
	 *            : Action to be performed on element
	 */
	public static boolean slider(WebElement slider, int x, int y) throws Throwable {

		boolean flag = false;
		try {
			// WebElement dragitem = driver.findElement(slider);
			// new Actions(driver).dragAndDropBy(dragitem, 400, 1).build()
			// .perform();
			new Actions(driver).dragAndDropBy(slider, x, y).build().perform();// 150,0
			Thread.sleep(5000);
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		}
	}

	public static boolean rightclick(WebElement elementToRightClick, String locatorName) throws Throwable {

		boolean flag = false;
		try {
			Actions clicker = new Actions(driver);
			clicker.contextClick(elementToRightClick).perform();
			flag = true;
			return true;

		} catch (Exception e) {

			return false;
		}
	}

	/**
	 * This method returns check existence of element
	 * 
	 * @param locator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:Textbox, checkbox etc)
	 * @return: Boolean value(True or False)
	 * @throws NoSuchElementException
	 */
	public static boolean isElementPresent(WebElement element) throws Throwable {
		boolean flag = false;
		try {
			// driver.findElement(by);
			flag = true;
			return true;
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}
	}

	/**
	 * @return: return title of current page.
	 * 
	 * @throws Throwable
	 */

	/*
	 * public static String getTitle() throws Throwable {
	 * 
	 * String text = driver.getTitle(); if (text) {
	 * 
	 * Log.message("GetTitle: " + b + " " + "Title Present On the Text" );
	 * 
	 * } return text; }
	 */

	public static boolean refreshPage() throws Throwable {
		boolean flag = false;
		try {
			driver.navigate().refresh();
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	public static boolean isElementDisplayed(WebElement element) throws Throwable {
		boolean flag = false;
		try {
			for (int i = 0; i < 200; i++) {
				if (element.isDisplayed()) {
					flag = true;
					break;
				} else {
					Thread.sleep(50);
				}
			}
		} catch (Exception e) {
			return false;
		}
		return flag;
	}

	public static void waitForJSandJQueryToLoad() {

		Boolean isJqueryUsed = (Boolean) ((JavascriptExecutor) driver)
				.executeScript("return (typeof(jQuery) != 'undefined')");
		if (isJqueryUsed) {
			while (true) {
				// JavaScript test to verify jQuery is active or not
				Boolean ajaxIsComplete = (Boolean) (((JavascriptExecutor) driver)
						.executeScript("return jQuery.active == 0"));
				if (ajaxIsComplete)
					break;
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
				}
			}
		}
	}

	
}