package com.arc.projects.pages;

import java.awt.AWTException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.RobotUtils;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

public class Search_Testcases extends LoadableComponent<Search_Testcases> {

	WebDriver driver;

	private boolean isPageLoaded;

	@FindBy(css = ".nav.navbar-nav.navbar-left>li>input")
	WebElement chkboxAllPhoto;

	@FindBy(css = ".dropdown.has-tooltip>a")
	WebElement btnDropdownMultPhotoSel;

	@FindBy(xpath = "(//button[@class='btn btn-primary btn-lg'])[2]")
	WebElement btnUploadPhotos1;

	@FindBy(xpath = "//input[@name='qqfile']")
	WebElement btnChooseFile;

	@FindBy(css = "#btnFileUpload")
	WebElement btnFileUpload;

	@FindBy(xpath = "//button[@class='btn btn-primary' and @data-bind='click: SaveAttributes']")
	WebElement btnSaveAttributes;

	@FindBy(xpath = "//em[@class='pull-left pw-icon-orange']")
	WebElement headerMsgUploadPhotoWindow;

	@FindBy(css = "#aPrjAddFolder")
	WebElement btnAddNewFolder;

	@FindBy(css = "#txtSearchKeyword")
	WebElement txtSearchField;

	@FindBy(css = "#btnSearch")
	WebElement btnGlobalSearch;

	@FindBy(css = "#txtFolderName")
	WebElement txtBoxFolderName;

	@FindBy(css = ".chkExcludedrawing")
	WebElement chkBoxExcludedrawing;

	@FindBy(css = ".chkExcludedrawing")
	WebElement chkBoxExcludeLD;

	@FindBy(css = "#btnFolderCreate")
	WebElement btnCreateFolder;

	@FindBy(css = "#btnFolderCreateThisFolder")
	WebElement btnUploadFilesToFolder;

	@FindBy(css = ".noty_text")
	WebElement notificationMsg;

	@FindBy(css = "#FeedbackClose>span")
	WebElement FeedBackmeesage;

	@FindBy(css = "#button-1")
	WebElement YesButton;

	@FindBy(xpath = "//span [@class='img-circle profile-no-image md']")
	WebElement Profile;
	
	@FindBy(xpath = "//i [@class='icon icon-user']")
	WebElement MYProfile;
	
	@FindBy(css="#add-project")
	WebElement btnCreateProject;
	
	@FindBy(css="#btnCreate")
	//@CacheLookup - Removed due to fail
	private WebElement btnCreate;
	
	@FindBy(css="#txtProjectName")
	//@CacheLookup - Removed due to fail
	private WebElement txtProjectName;
	
	@FindBy(css="#txtProjectNumber")
	WebElement txtProjectNumber;
	
	@FindBy(xpath=".//*[@id='prj-start-date']/span")
	WebElement butProjectStartDate;
	
	@FindBy(css="#txtProjectDescription")
	WebElement txtProjectDescription;
	
	@FindBy(css="#txtAddress")
	WebElement txtAddress;
	
	@FindBy(css="#txtProjectStartDate")
	WebElement txtProjectStartDate;
	
	@FindBy(css="#txtCity")
	WebElement txtCity;
	
	@FindBy(css="#txtState")
	WebElement txtState;
	
	@FindBy(css="#txtZip")
	WebElement txtZip;
	
	@FindBy(css="#txtCountry")
	WebElement txtCountry;
	
	@FindBy(css="#txtProjectPassword")
	WebElement txtProjectPassword;
	
	@FindBy(css=".noty_text")
	WebElement projectCreationSuccessMsg;
	
	@FindBy(css=".Country-Item[title='USA']")
	WebElement countryTitleItem;
	
	@FindBy(css=".State-Item[data-name='alabama']")
	WebElement stateTitleItem;
	
	@FindBy(css="#isAutoHyperlinkEnabled")
	WebElement checkboxEnableAutoHypLink;
	
	@FindBy(xpath="(//button[@class='btn btn-default pull-right'])[4]")
	WebElement btnSavePrjSettingsWin;
	
	@FindBy(xpath=".//*[@id='breadCrumb']/div[2]/ul/li[3]")
	WebElement prjNameBreadcrum;
	
	@FindBy(xpath="//i[@class='icon icon-app-project icon-lg pw-icon-white pulse animated']")
	WebElement projectDasboadLink;
	
	
	@FindBy(css="#button-1")
	WebElement btnYes;
	
	@FindBy(css=".popover-content")
	WebElement contentPasswordField;

	@FindBy(xpath = "(//a[@onclick='javascript:return confirmlogout(this);'])[2]")
	WebElement Logout;

	@FindBy(xpath = "//i[@class='icon icon-app-project icon-2x pulse animated']")
	WebElement ProjectManagement;

	@FindBy(css = "#a_showRFIList")
	WebElement ProjectManagement_RFI;
	
	@FindBy(css = "#a_showSUBMITTALList")
	WebElement ProjectManagement_Submital;
	
	// ======project search filter==============
	
	
	@FindBy(xpath = "//*[@id='txtProjectName_filter']")
	WebElement Filter_ProjectName;
	
	@FindBy(xpath = "//*[@id='txtProjectNumber_filter']")
	WebElement Filter_ProjectNumber;
	
	@FindBy(xpath = "//*[@id='txtProjectDesc_filter']")
	WebElement Filter_Description;
	
	@FindBy(xpath = "//*[@id='txtProjectCity']")
	WebElement Filter_CITY;
	
	@FindBy(xpath = "//*[@id='txtProjectState']")
	WebElement Filter_State;
	
	@FindBy(xpath = "//*[@id='txtProjectCountry']")
	WebElement Filter_Country;
	

	//================== submittal========
	
	@FindBy(css = ".new-submittal.dev-new-submittal")
	WebElement createLink_Submital;
	
	@FindBy(css = ".new-submittal.dev-new-submittal")
	WebElement createSubmital_Link;
	//=============================================
	
	@FindBy(css = "#txtSubmittalNumber_filter")
	WebElement Submital_NumberInbox_filter;
	
	@FindBy(css = "#txtSubmittalName_filter")
	WebElement Submital_NameInbox_filter;
	
	@FindBy(css = "#ddlSubmittalType_filter")
	WebElement Submital_Type_filter;
	
	@FindBy(css = ".btn.btn-default.dropdown-toggle")
	WebElement Submital_Status_filter_dropdown;
	
	@FindBy(xpath = "//input[@value='Open!0']")
	WebElement Submital_Status_filter_OpenStatus;
	
	@FindBy(xpath = "//span [@class='icon-calender icon-calendar icon']")
	WebElement Submital_Duedate_filter_Calender;	

	@FindBy(css = "#txtSubmittalNumber")
	WebElement Submital_Duedate_filter;
	
	@FindBy(xpath = "(//a [@data-toggle='tooltip'])[14]")
	WebElement EmailText;
	
	
	@FindBy(xpath = "(//h5 [@data-toggle='tooltip'])[2]")
	WebElement phoneText;
	
	
	
	
	
	//======create submittal page=====
	
	@FindBy(css = "#txtSubmittalNumber")
	WebElement Submital_NumberInbox;
	
	@FindBy(css = "	#txtSubmittalName")
	WebElement Submital_NameInbox;
	
	@FindBy(css = "#txtSubmittalDueDate")
	WebElement Submital_duedateInbox;	
	
	@FindBy(css = ".btn.btn-default.dropdown-toggle")
	WebElement Submital_type;
	
	@FindBy(xpath = "(//a [@class='SubmittaltypeList'])[1]")
	WebElement Submital_List1;
	
	
	@FindBy(xpath = "(//button [@id='btnCreateSubmittal'])[1]")
	WebElement CreateAndSubmit_Button;
	
	
	@FindBy(xpath = "(//button [@id='btnCreateSubmittal'])[1]")
	WebElement Create_Button;
	
	@FindBy(css = "#txtSubmittalNumber")
	WebElement txtSubmittalNumber;

	@FindBy(css = "#txtSubmittalName")
	WebElement txtSubmittalName;

	@FindBy(css = "#txtSubmittalDueDate")
	WebElement txtSubmittalDueDate;

	@FindBy(xpath = "(//button[@class='btn btn-default dropdown-toggle'])[2]")
	WebElement dropDownSubmittalType;
	
	@FindBy(xpath = "//*[@id='ddlSubmittalType_filter']")
	WebElement dropDownSubmittalType_filter;
	
	
	@FindBy(xpath = "//a[@class='SubmittaltypeList' and @data-name='Bonds']")
	WebElement optionSubmittalTypeSelect;

	@FindBy(xpath = "(//button[@class='btn btn-default' and @data-target='.company-list'])[1]")
	WebElement btnCompanyList;
	
	@FindBy(xpath = "(//button[@class='btn btn-default' and @data-target='.company-list'])[2]")
	WebElement btnCompanyList2;

	@FindBy(xpath = "(//input[@type='radio'])[1]")
	WebElement radiobtnSelectCompany;
			
	@FindBy(xpath = "(//button[@id='btnCreateSubmittal'])[2]")
	WebElement btnCreateSubmittal;

	@FindBy(css = "#button-0")
	WebElement btnNo_WanttoCreateSubmittal;

	@FindBy(css = "#button-0")
	WebElement btnOK_appliedToAllImpSubmittals;
	
	@FindBy(css = "#button-1")
	WebElement btnYes_WanttoCreateSubmittal;

	@FindBy(css = "#button-2")
	WebElement btnYes_WithCoverPage;
	

	@FindBy(xpath = "(//span[@class='sbmtl-id'])[1]")
	WebElement submIdUnderList;

	@FindBy(xpath = "(//span[@class='sbmtl-nm'])[1]")
	WebElement submNameUnderList;

	@FindBy(xpath = "(//span[@class='sbmtl-type'])[1]")
	WebElement submTypeUnderList;

	@FindBy(xpath = "(//span[@class='text-info'])[1]")
	WebElement submDueDateUnderList;

	@FindBy(xpath = "(//span[@class='yellow-txt'])[1]")
	WebElement submOpenStatusUnderList;
	
	@FindBy(xpath = "(//i [@class='icon icon-app-contact icon-lg'])[2]")
	WebElement ASSIGNEDTo;
	
	
	@FindBy(xpath = "(//i [@class='icon icon-app-contact icon-lg'])[3]")
	WebElement CC;
	

	@FindBy(xpath = "(//span[@data-toggle='tooltip'])[3]")
	WebElement Assignee ;

	
	
	@FindBy(xpath = "//*[@id='txtSubmittalComment']")
	WebElement Comments_submittal;
	
	@FindBy(xpath = "//*[@id='btnCreatesubmittallist']")
	WebElement SubmitButton;
	
	

	//========================
	
	
	

	@FindBy(xpath = "//*[@id='btnSearchOptions']")
	WebElement Search_GlobalButton;

	@FindBy(xpath = "(//i [@class='icon icon-app-punch icon-lg'])[1]")
	WebElement Search_Punchdropdown;

	@FindBy(xpath = "//*[@id='txtSearchKeyword']")
	WebElement TxtSearch_Keyword;

	@FindBy(xpath = "//span [@data-bind='text: PunchDescription()']")
	WebElement Punch_Text;

	@FindBy(xpath = "(//span[@data-bind='text: Subject()'])[1]")
	WebElement RFI_Text;

	@FindBy(xpath = "(//span [@data-bind='text: RFINumber()'])[1]")
	WebElement RFI_Number;
	
	@FindBy(xpath = "(//span [@class='label label-warning'])[1]")
	WebElement RFI_Duedate_Text;
	
	
	@FindBy(xpath = "//i [@class='icon icon-app-contact icon-lg pw-icon-white']")
	WebElement Contacts;
	
	@FindBy(xpath = "//span [@data-toggle='tooltip' and @data-original-title='EMP1 EMP1']")
	WebElement Name;
	
	
	
	
	
	
	

	@FindBy(xpath = "//*[@id='aAdvanceFilter']")
	WebElement AdvanceSearch_Link;
	
	@FindBy(xpath = "//*[@id='txtContactFirstName_filter']")
	WebElement AdvanceSearch_FirstName;
	
	@FindBy(xpath = "//*[@id='txtContactLastName_filter']")
	WebElement AdvanceSearch_LastName;
	
	@FindBy(xpath = "//*[@id='txtContactEmail_filter']")
	WebElement AdvanceSearch_Email;
	
	@FindBy(xpath = "//*[@id='txtContactPhoneWork_filter']")
	WebElement AdvanceSearch_Phone;
	
	@FindBy(xpath = "//*[@id='txtContactCity_filter']")
	WebElement AdvanceSearch_City;
	
	@FindBy(xpath = "//*[@id='txtContactAddress_filter']")
	WebElement AdvanceSearch_Address;
	// Filter search punch=====================
	
	@FindBy(xpath = "//*[@id='txtPunchDescription_filter']")
	WebElement AdvanceSearch_PunchDescription;
	
	@FindBy(xpath = "//*[@id='txtPunchCreateBy_filter']")
	WebElement AdvanceSearch_PunchCreator;
	
	@FindBy(xpath = "//*[@id='txtPunchAssignee_filter']")
	WebElement AdvanceSearch_PunchAssignee;
	
	@FindBy(xpath = "//*[@id='txtPunchStatus_filter']")
	WebElement AdvanceSearch_PunchStatus;
	
	
	@FindBy(xpath = "(//span [@data-bind='text: PunchDescription()'])[1]")
	WebElement PunchDescription_text;
	
	@FindBy(xpath = "//*[@id='txtASCompany']")
	WebElement Company_text;
	
	
	@FindBy(xpath = "(//i [@class='icon icon-ellipsis-horizontal icon-lg'])[1]")
	WebElement projectMultipleObject;
	
	@FindBy(xpath = "//a [@class='edit-project-event']")
	WebElement projectMultipleObject_edit;
	
	@FindBy(xpath = "//*[@id='btnCancel']")
	WebElement project_CancelButton;
	
	
	
	
	
	
	
	

	
	@FindBy(xpath = "(//span [@data-bind='text: PunchDescription()'])[1]")
	WebElement Punchcreator_text;
	
	@FindBy(xpath = "(//span [@data-bind='text: PunchDescription()'])[1]")
	WebElement PunchAssignee_text;
	
	
	@FindBy(xpath = "(//span [@class='yellow-txt'])[1]")
	WebElement Punchstatus_text;
	
	@FindBy(xpath = "(//span [@class='yellow-txt'])[1]")
	WebElement submittalstatus_text;
	
	

	@FindBy(xpath = "	(//span[@data-toggle='tooltip'])[1]")
	WebElement Project_NameText;
	
	@FindBy(xpath = "html/body/div[1]/div[3]/div[3]/div/ul/li[1]/div/section[1]/p")
	WebElement Project_NumberText;
	
	@FindBy(xpath = ".//*[@id='txtProjectDescription']")
	WebElement Descriptiontext;
	
	@FindBy(xpath = "//*[@id='txtCity']")
	WebElement CITYtext;

	
	
	//==========Filter search Punch end================================
	

	@FindBy(xpath = "(//span[@ class='icon-calender icon-calendar icon'])")
	WebElement calender_Link;

	@FindBy(xpath = "//*[@id='txtRFINumber_filter']")
	WebElement Filter_RFINumber;

	@FindBy(xpath = "//*[@id='txtRFINumber_filter']")
	WebElement Filter_RFIDueDate;

	@FindBy(xpath = "//*[@id='txtAssignBy_filter']")
	WebElement Filter_Assignee;

	@FindBy(xpath = "//*[@id='txtCreateBy_filter']")
	WebElement Filter_Creator;

	@FindBy(xpath = "//*[@id='txtStatus_filter']")
	WebElement Filter_Status1;

	@FindBy(xpath = "//*[@id='txtCreateBy_filter']")
	WebElement Filter_Status;

	@FindBy(xpath = "//*[@placeholder='Building']")
	WebElement Building;

	@FindBy(xpath = "//*[@placeholder='Level']")
	WebElement Level;

	@FindBy(xpath = "//*[@placeholder='Room']")
	WebElement room;

	@FindBy(xpath = "//*[@placeholder='Area']")
	WebElement Area;

	@FindBy(xpath = "//*[@placeholder='Description']")
	WebElement Description;

	@FindBy(xpath = "//input [@id='txtStatus_filter']")
	WebElement Status_Inbox;

	@FindBy(xpath = "//*[@id='txtPunchDescription_filter']")
	WebElement filterSearch_DescriptionName;

	@FindBy(xpath = "//*[@id='txtProjectName_filter']")
	WebElement FilterProject_Name;

	@FindBy(css = "#btnSearchFilter")
	WebElement SearchButton_Advance;

	@FindBy(css = "#btnSearch")
	WebElement Search_Button;

	@FindBy(xpath = "//button[@class='btn btn-primary' and @data-bind='click: SelectContactEvent']")
	WebElement SelectUser_Button;

	@FindBy(css = "#liWhereIAmAll>a")
	WebElement Everywhere_AllContents;

	@FindBy(css = "#liWhereIAmSearchIn>a")
	WebElement search;

	@FindBy(xpath = "(//span[ @data-toggle='tooltip'])[1]")
	WebElement Staticproject;
	
	@FindBy(xpath = "(//span [@class='sbmtl-id'])[1]")
	WebElement SubmittalN0;	
	
	@FindBy(xpath = "(//i[@class='icon icon-submittal-status-icon icon-3x pw-icon-grey'])[1]")
	WebElement FirstSubmittallist;
	
	@FindBy(xpath = "//span [@class='truncate-sm']")
	WebElement submittalTypeText;
	
	@FindBy(xpath = "//*[@id='divEditSubmittalUI']/div/div[3]/button[1]")
	WebElement CloseSubmittalwindow;
	
	
	
	
	
	
	@FindBy(xpath = "(//span[@ class='sbmtl-nm'])[1]")
	WebElement SubmittalName;

	@FindBy(xpath = "//*[@id='txtSubmittalNumber_filter']")
	WebElement Filter_SubmittalNumber;	
	
	
	@FindBy(xpath = "//*[@id='txtSubmittalName_filter']")
	WebElement Filter_SubmittalName;
	
	@FindBy(xpath = ".//*[@id='ddlSubmittalType_filter']")
	WebElement Filter_SubmittalType;
	
	
	@FindBy(xpath = ".//*[@id='submittalStatusSelectToggle']/button")
	WebElement Filter_SubmittalStatus;
	
	
	@FindBy(xpath = "//span [@class='icon-calender icon-calendar icon']")
	WebElement Filter_Submittalduedate;
	
	

	@FindBy(xpath = "//h4[@data-placement='top']")
	WebElement folder_Number;

	@FindBy(xpath = "//h4 [@class='pw-file-name']")
	WebElement FileName;

	@FindBy(css = ".media.navbar-collapse>section>h4>span")
	WebElement FolderName_photo;

	@FindBy(xpath = "//*[@class='day old' and text()='31']")
	WebElement old31;

	@FindBy(xpath = "//*[@class='day' and text()='31']")
	WebElement new31;

	@FindBy(xpath = "//*[@class='day' and text()='29']")
	WebElement feb29;
	
	@FindBy(xpath = ".//*[@class='day' and text()='%s']")
	WebElement expDate;

	// =======================================================

	@Override
	protected void load() {
		isPageLoaded = true;

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}

	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * 
	 * @param driver
	 */
	public Search_Testcases(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public boolean ObjectFeedback() {
		SkySiteUtils.waitForElement(driver, FeedBackmeesage, 20);
		Log.message("Waiting for FreeTrail Link to be appeared");
		if (FeedBackmeesage.isDisplayed())
			return true;
		else
			return false;
	}
	
	
	
	
	public boolean Create_Submital_AndValidate(String Submittal_Number,String Submittal_Name) 
	
		{
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, createSubmital_Link, 60);
		if (createSubmital_Link.isDisplayed()){
			createSubmital_Link.click();
		Log.message("Submittal Button Clicked Sucessfully");
		}
		SkySiteUtils.waitForElement(driver, btnCreateSubmittal, 20);
		txtSubmittalNumber.sendKeys(Submittal_Number);
		txtSubmittalName.sendKeys(Submittal_Name);
		txtSubmittalDueDate.click();// Click on due date
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("html/body/div[10]/div/div[1]/table/tbody/tr[5]/td[7]")).click();// Select due date from cal
		SkySiteUtils.waitTill(2000);
		dropDownSubmittalType.click();
		SkySiteUtils.waitTill(2000);
		optionSubmittalTypeSelect.click();
		Log.message("Selected submittal Type from dropdown.");
		
		//Getting number of approvers
		int Avl_Approvers_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//button[@class='btn btn-default' and @data-target='.company-list']")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_Approvers_Count = Avl_Approvers_Count+1; 	
		} 
		Log.message("Available Approver Count is: "+Avl_Approvers_Count);
		
		if(Avl_Approvers_Count==1)
		{
			btnCompanyList.click();
			SkySiteUtils.waitForElement(driver, radiobtnSelectCompany, 20);
			SkySiteUtils.waitTill(2000);
			radiobtnSelectCompany.click();
			Log.message("Selected first company radio button.");
		}
		else if(Avl_Approvers_Count==2)
		{
			btnCompanyList.click();
			SkySiteUtils.waitForElement(driver, radiobtnSelectCompany, 20);
			SkySiteUtils.waitTill(2000);
			radiobtnSelectCompany.click();
			Log.message("Selected first company radio button.");
			SkySiteUtils.waitTill(2000);
			btnCompanyList2.click();
			SkySiteUtils.waitForElement(driver, radiobtnSelectCompany, 20);
			SkySiteUtils.waitTill(2000);
			radiobtnSelectCompany.click();
			Log.message("Selected first company radio button.");
		}
		SkySiteUtils.waitTill(2000);
		btnCreateSubmittal.click();
		Log.message("Clicked on create submittal button.");
		//SkySiteUtils.waitForElement(driver, btnNo_WanttoCreateSubmittal, 20);
		//btnYes_WanttoCreateSubmittal.click();
		//Log.message("Clicked on button 'YES' for conform to create submittal.");
		//SkySiteUtils.waitTill(1000);
		//SkySiteUtils.waitForElement(driver, notificationMsg, 20);
		//String NotifyMsg_SubmitalCreate=notificationMsg.getText();
		//Log.message("Message displayed after submittal create is: "+NotifyMsg_SubmitalCreate);
		SkySiteUtils.waitForElement(driver, submIdUnderList, 20);
		SkySiteUtils.waitTill(5000);
		String SubmId_AllSubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_AllSubTab);
		String SubmName_AllSubTab = submNameUnderList.getText();
		Log.message("Sub Name is: " + SubmName_AllSubTab);
		//String SubmType_AllSubTab = submTypeUnderList.getText();
		//Log.message("Sub Type is: " + SubmType_AllSubTab);
		String SubmStatus_AllSubTab = submOpenStatusUnderList.getText();
		Log.message("Sub status is: " + SubmStatus_AllSubTab);

		if ((SubmId_AllSubTab.equalsIgnoreCase(Submittal_Number)) && (SubmName_AllSubTab.equalsIgnoreCase(Submittal_Name))
				&& (SubmStatus_AllSubTab.equalsIgnoreCase("OPEN"))) 
		{
			Log.message("Created Submittal is successfully.");
			SkySiteUtils.waitTill(8000);
			return true;
		} else {
			Log.message("Created Submittal is failed.");
			return false;
		}
	}

	public void SelectGalleryFolder(String Gallery) {
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//li[@class='media navbar-collapse']"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		for (int j = 1; j <= Avl_Fold_Count; j++) {
			// actualFolderName =
			// driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["
			// + j + "]/section[2]/h4")).getText();
			actualFolderName = driver.findElement(By.xpath("//li[@class='media navbar-collapse']")).getText();

			Log.message("Act Name:" + actualFolderName);
			SkySiteUtils.waitTill(5000);
			// Validating the new Folder is created or not
			if (actualFolderName.trim().contentEquals(Gallery)) {
				driver.findElement(By.xpath("(//li[@class='media navbar-collapse'])[" + j + "]")).click();// Select
																											// Gallery
																											// folder
				Log.message("Clicked on Gallery Folder");
				SkySiteUtils.waitTill(5000);
				break;
			}
		}

	}

	public boolean Logout() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		Log.message("Entered into Logout Method");
		SkySiteUtils.waitForElement(driver, Profile, 60);
		Profile.click();
		Log.message("Profile Option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Logout, 60);
		SkySiteUtils.waitTill(2000);
		Logout.click();
		Log.message("Logout option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();

		return result;

	}

	public ProjectDashboardPage YesButton_Alert() throws Throwable

	{
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();
		SkySiteUtils.waitTill(2000);
		String actualTitle = "Sign in - SKYSITE";
		String expectedTitle = driver.getTitle();
		Log.message("expected title is:" + expectedTitle);
		return new ProjectDashboardPage(driver).get();

	}

	private boolean isFileDownloaded_Ext(String dirPath, String ext) {
		boolean flag = false;
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			flag = false;
		}

		for (int i = 1; i < files.length; i++) {
			if (files[i].getName().contains(ext)) {
				flag = true;
			}
		}
		return flag;
	}

	public boolean isFileDownloaded(String downloadPath, String fileName) {
		boolean flag = false;
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();

		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().equals(fileName))
				return flag = true;
		}

		return flag;
	}

	public boolean ProjectManagement() throws Throwable

	{
		boolean result = true;
		Thread.sleep(2000);
		SkySiteUtils.waitForElement(driver, ProjectManagement, 60);
		ProjectManagement.click();
		Thread.sleep(2000);
		Log.message("project Management Link Click Sucessfully");
		SkySiteUtils.waitForElement(driver, ProjectManagement_RFI, 60);
		ProjectManagement_RFI.click();

		Log.message("RFI Link Click Sucessfully");

		return result;

	}

	public boolean NewgallaryFolder_Create(String Foldername) {
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 60);
		Log.message("Waiting for Create Folder button to be appeared");
		SkySiteUtils.waitTill(3000);
		btnAddNewFolder.click();// Clicking on Create New Folder
		Log.message("Add New folder button clicked!!!");
		SkySiteUtils.waitForElement(driver, btnUploadFilesToFolder, 60);
		SkySiteUtils.waitTill(5000);
		txtBoxFolderName.sendKeys(Foldername);// Giving Folder Names Randomly
		Log.message("Folder Name: " + Foldername + " has been entered in Folder Name text box.");
		btnCreateFolder.click();
		Log.message("Create Folder button clicked!!!");
		SkySiteUtils.waitForElement(driver, notificationMsg, 60);
		String Msg_Folder_Create = notificationMsg.getText();
		Log.message("Notification Message after folder create is: " + Msg_Folder_Create);
		SkySiteUtils.waitTill(5000);
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		for (int j = 1; j <= Avl_Fold_Count; j++) {
			actualFolderName = driver
					.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4"))
					.getText();
			Log.message("Exp Name:" + Foldername);
			Log.message("Act Name:" + actualFolderName);

			// Validating the new Folder is created or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) {
				Log.message("Folder is created successfully with name: " + Foldername);
				break;
			}
		}

		// Final Validation - Folder Create
		if ((Foldername.trim().contentEquals(actualFolderName.trim()))
				&& (Msg_Folder_Create.contentEquals("Folder created successfully"))) {
			Log.message("Folder is created successfully with name: " + Foldername);
			return true;
		} else {
			Log.message("Folder creattion FAILED with name: " + Foldername);
			return false;
		}

	}

	public boolean UploadPhotos_InGallery(String FolderPath, int FileCount)
			throws InterruptedException, AWTException, IOException {
		boolean result = false;
		SkySiteUtils.waitForElement(driver, btnUploadPhotos, 30);
		Log.message("Waiting for upload file button to be appeared");
		btnUploadPhotos.click();// Click on Upload photos
		Log.message("Clicked on upload photos button.");
		SkySiteUtils.waitForElement(driver, headerMsgUploadPhotoWindow, 40);
		Log.message("Waiting for choose file button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();// Click on Choose File
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(5000);

		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String Sys_Download_Path = "./File_Download_Location/" + rn.nextFileName() + ".txt";
		Log.message("system download location2" + Sys_Download_Path);
		String tmpFileName = Sys_Download_Path;
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;

		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(6000);
			}
		}

		output.flush();
		output.close();
		SkySiteUtils.waitTill(3000);
		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		File dest = new File(FolderPath);
		String path = dest.getAbsolutePath();
		SkySiteUtils.waitTill(5000);
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + path + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(15000);
		SkySiteUtils.waitForElement(driver, btnFileUpload, 60);
		SkySiteUtils.waitTill(5000);
		// Delete the temp file
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}

		SkySiteUtils.waitTill(5000);
		btnFileUpload.click();
		Log.message("Clicked on Upload button.");
		SkySiteUtils.waitForElement(driver, btnSaveAttributes, 180);
		Log.message("Waiting for Save attributes button to be appeared.");
		SkySiteUtils.waitTill(5000);

		// Validations on Add Attributes page
		String Photo_Information = driver.findElement(By.xpath("//h4[@class='pull-left']")).getText();// Getting
																										// count
		Log.message(Photo_Information);
		// Split the line and get the count
		Splitter SPLITTER = Splitter.on(CharMatcher.whitespace()).omitEmptyStrings();
		List<String> elements = Lists.newArrayList(SPLITTER.split(Photo_Information));
		// Getting count - output
		String OnlyCount = elements.get(3);
		Log.message("The required count value is: " + OnlyCount);
		// Convert String to int
		int PhotosCount = Integer.parseInt(OnlyCount);
		int PageCount = PhotosCount / 10;
		int Extra_Photo = PhotosCount % 10;
		int totalPageCount = 0;

		// Getting Page count
		if ((PageCount > 0) && (Extra_Photo > 0))// Multiple of 10 plus extra
		{
			totalPageCount = PageCount + 1;
		}

		if ((PageCount > 0) && (Extra_Photo == 0))// Multiple of 10 only
		{
			totalPageCount = PageCount;
		}

		if ((PageCount == 0) && (Extra_Photo < 10))// less than 10 only
		{
			totalPageCount = 1;
		}

		Log.message("Total Page Count is:" + totalPageCount);

		for (int i = 1; i <= totalPageCount; i++) {
			if ((PhotosCount < 10) && (PhotosCount == FileCount)) {
				// Fill the Attributes and use the copy down
				String Building = PropertyReader.getProperty("Building");
				driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).sendKeys(Building);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[1]")).click();

				String Level = PropertyReader.getProperty("Level");
				driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).sendKeys(Level);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[2]")).click();

				String room = PropertyReader.getProperty("Room");
				driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).sendKeys(room);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[3]")).click();

				String area = PropertyReader.getProperty("Area");
				driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).sendKeys(area);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[4]")).click();

				String Description = PropertyReader.getProperty("Description");
				driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).sendKeys(Description);

				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[5]")).click();
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath(".//*[@id='divAttrContainer']/div[1]/div[2]/div/div[2]/button")).click();
				SkySiteUtils.waitTill(5000);

			}
			if ((PhotosCount >= 10) && (PhotosCount == FileCount)) {
				// Fill the Attributes and use the copy down
				String Building = PropertyReader.getProperty("Building");
				driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).sendKeys(Building);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[1]")).click();

				String Level = PropertyReader.getProperty("Level");
				driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).sendKeys(Level);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[2]")).click();

				String room = PropertyReader.getProperty("Room");
				driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).sendKeys(room);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[3]")).click();

				String area = PropertyReader.getProperty("Area");
				driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).sendKeys(area);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[4]")).click();
				String Description = PropertyReader.getProperty("Description");
				driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).sendKeys(Description);

				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[5]")).click();
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath(".//*[@id='divAttrContainer']/div[1]/div[2]/div/div[2]/button")).click();
				SkySiteUtils.waitTill(5000);
			}

		} // end of i for loop
		SkySiteUtils.waitTill(10000);

		// Calling Upload Photo validation script
		result = this.ValidateUploadPhotos(FolderPath, FileCount);// Calling
																	// validate
																	// photos
																	// method
		if (result == true) {
			Log.message("Photos Upload is working successfully!!!");
			return true;
		} else {
			Log.message("Photos Upload is not working!!!");
			return false;
		}

	}

	public boolean ValidateUploadPhotos(String FolderPath, int FileCount) throws InterruptedException {
		boolean result = false;
		String ActPhotoName = null;
		String expPhotoname = null;
		int uploadSuccessCount = 0;

		try {
			// Reading all the photo names in a folder
			File[] files = new File(FolderPath).listFiles();
			for (File file : files) {
				if (file.isFile()) {
					expPhotoname = file.getName();// Getting Photo Names into a
													// variable
					Log.message("Expected Photo name is:" + expPhotoname);
					SkySiteUtils.waitTill(1000);
					for (int n = 1; n <= FileCount + (FileCount - 1); n++) {
						n = n + 1;
						ActPhotoName = driver
								.findElement(By.xpath(
										"html/body/div[1]/div[4]/div[2]/div/div/div/ul/li[" + n + "]/section[2]/h4"))
								.getText();
						Log.message("Actual Photo name is:" + ActPhotoName);

						if (expPhotoname.trim().contains(ActPhotoName.trim())) {
							uploadSuccessCount = uploadSuccessCount + 1;
							Log.message(
									"File uploded success for:" + ActPhotoName + "Sucess Count:" + uploadSuccessCount);
							break;
						}
					}

				}

			}

			// Checking whether file count is equal or not
			if (FileCount == uploadSuccessCount) {
				result = true;
			} else {
				result = false;
			}

		} // End of try block

		catch (Exception e) {
			result = false;
		}

		finally {
			return result;
		}
	}

	public boolean Searchscenario_RFI() throws Throwable {
		boolean result = true;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Module Search Option Click Sucessfully");
		/*
		 * SkySiteUtils.waitForElement(driver, Search_Punchdropdown, 120);
		 * Search_Punchdropdown.click();
		 */
		Log.message("Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		TxtSearch_Keyword.clear();
		SkySiteUtils.waitTill(2000);
		String Anything = RFI_Text.getText();
		TxtSearch_Keyword.sendKeys(Anything);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		String description = PropertyReader.getProperty("RFI_Subject");
		if (Anything.contentEquals(description)) {
			Log.message("Maching RFI Found!!");

		} else {

			Log.message("Matching RFI Not Found!!");

		}

		return result;

	}

	public boolean Searchscenario_punch() throws Throwable {
		boolean result = true;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Local Search Option Click Sucessfully");
		/*
		 * SkySiteUtils.waitForElement(driver, Search_Punchdropdown, 120);
		 * Search_Punchdropdown.click();
		 */
		Log.message("Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		TxtSearch_Keyword.clear();
		SkySiteUtils.waitTill(2000);
		String Anything = Punch_Text.getText();
		TxtSearch_Keyword.sendKeys(Anything);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");

		String description = PropertyReader.getProperty("Punch_Subject");
		if (Anything.trim().contentEquals(description.trim())) {
			Log.message("Maching Punch Found!!");

		} else {

			Log.message("Matching Punch Not Found!!");

		}

		return result;

	}

	public boolean Searchscenario(String Anything) throws Throwable {
		boolean result = true;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Local Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		search.click();
		Log.message("Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		TxtSearch_Keyword.clear();
		SkySiteUtils.waitTill(2000);
		TxtSearch_Keyword.sendKeys(Anything);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(15000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(20000);
		Search_Button.click();
		return result;

	}
	
	
	

	
	
	
	
	
	public boolean SubmittalStatus_Validation(String Anything) throws Throwable {
		boolean result = true;
		SkySiteUtils.waitTill(5000);
		String Status = submOpenStatusUnderList.getText();
		Log.message("Status is:" + Status);
		if (Status.trim().contentEquals(Anything.trim())) {
			Log.message("Maching Status Found!!");

		} else {

			Log.message("Matching Status Not Found!!");

		}
		return result;

	}


	public boolean Searchprojectvalidation(String Anything) throws Throwable

	{
		boolean result = true;

		SkySiteUtils.waitTill(5000);

		String Exp_ProjName = Staticproject.getText();
		Log.message("Project name is:" + Exp_ProjName);

		if (Exp_ProjName.trim().contentEquals(Anything.trim())) {
			Log.message("Maching Project Found!!");

		} else {

			Log.message("Matching project Not Found!!");

		}
		return result;

	}

	public boolean SearchFoldervalidation(String Anything) throws Throwable

	{
		boolean result = true;

		SkySiteUtils.waitTill(5000);

		String Exp_folderName = folder_Number.getText();
		Log.message("Folder name is:" + Exp_folderName);

		if (Exp_folderName.trim().contentEquals(Anything.trim())) {
			Log.message("Maching folder Found!!");

		} else {

			Log.message("Matching Folder Not Found!!");

		}
		return result;

	}

	public boolean Searchfolderphotovalidation(String Anything) throws Throwable

	{
		boolean result = true;

		SkySiteUtils.waitTill(5000);

		String FileName1 = FolderName_photo.getText();
		Log.message("File name is:" + FileName1);

		if (FileName1.contains(Anything.trim())) {
			Log.message("Matching photo Folder  Found!!");

		} else {

			Log.message("Matching photo Folder  Not Found!!");

		}
		return result;

	}

	public boolean SearchFilevalidation(String Anything) throws Throwable

	{
		boolean result = true;

		SkySiteUtils.waitTill(5000);

		String FileName1 = FileName.getText();
		Log.message("File name is:" + FileName1);

		if (FileName1.contentEquals(Anything.trim())) {
			Log.message("Maching File Found!!");

		} else {

			Log.message("Matching File Not Found!!");

		}
		return result;

	}

	public boolean AdvanceSearch_building(String building) throws Throwable {
		boolean result = false;
		Log.message("Enter Into  filter By search status");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, FilterProject_Name, 60);
		SkySiteUtils.waitTill(3000);
		Building.sendKeys(building);
		Log.message("Entered The Value In search Inbox Text Field: - " + building);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 60);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		return result;

	}
	
	
	
	
	
	
	
	
	
	
	public boolean AdvanceSearch_Status(String SubmittalName) throws Throwable {
		boolean result = false;
		Log.message("Enter Into  filter By search status");
		SkySiteUtils.waitTill(500);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Submital_NumberInbox_filter, 60);
		SkySiteUtils.waitTill(3000);
		Submital_NameInbox_filter.sendKeys(SubmittalName);
		Log.message("Entered The Value In search Inbox Text Field: - " + SubmittalName);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 60);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		return result;

	}
	
	
	public boolean AdvanceSearch_SubmittalType() throws Throwable {
		boolean result = false;
		Log.message("Enter Into  filter By search status");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");		
		Select Dropdown = new Select(driver.findElement(By.xpath("//*[@id='ddlSubmittalType_filter']")));
		Dropdown.selectByVisibleText("LEED");	
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 60);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		return result;

	}
	
	
	
	/*public static void ytyut(WebElement element, String Value){
		ele.sendKeys("hkjhjfd");
		
	}*/
	
	       
	
	public boolean Contact_FirstName() throws Throwable {
		boolean result = false;
		SkySiteUtils.waitForElement(driver, Contacts, 60);
		Contacts.click();
		/*String Contact_FirstName = PropertyReader.getProperty("Contact_FirstName");
		
		String Cont_FirstName = Name.getText();
		String[] parts = Cont_FirstName.split(" ");
		String part1 = parts[0]; // 004
		
		if (part1.trim().contentEquals(Contact_FirstName.trim())) {
			Log.message("First Name Verified Sucessfully");	

		} else {
			Log.message("First Name Not Verified Sucessfully");

		}
		*/
		return result;
	
	
	}
	        
	        public boolean Contact_LastName() throws Throwable {
	    		boolean result = false;
	    		SkySiteUtils.waitForElement(driver, Contacts, 60);
	    		Contacts.click();
	    		String Contact_FirstName = PropertyReader.getProperty("Contact_LastName");
	    		
	    		String Cont_FirstName = RFI_Number.getText();
	    		String[] parts = Cont_FirstName.split(" ");
	    		String part2 = parts[1]; // 004
	    		
	    		if (part2.trim().contentEquals(Contact_FirstName.trim())) {
	    			Log.message("First Name Verified Sucessfully");	

	    		} else {
	    			Log.message("First Name Not Verified Sucessfully");

	    		}
	    		
	    		return result;
	    	
	    	
	    	}
	        
	        
	        
	        public boolean AdvanceSearch_Project(String CommonValue,String Flag) throws Throwable {
	    		boolean result = false;
	    		SkySiteUtils.waitTill(2000);
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
	    		AdvanceSearch_Link.click();
	    		Log.message("Advance Search Option Clicked Sucessfully");	    		
	    		SkySiteUtils.waitTill(5000);
	    		if(Flag.equalsIgnoreCase("Search_ProjectName")){
	    		SkySiteUtils.waitTill(5000);
	    		SkySiteUtils.waitForElement(driver, Filter_ProjectName, 120);
	    		Filter_ProjectName.sendKeys(CommonValue);
	    		Log.message("Entered the Value In Project Name field:-"+CommonValue);	
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SkySiteUtils.waitTill(40000);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");	    			    		
	    		SkySiteUtils.waitTill(5000);
	    		String ProjectName = Project_NameText.getText();	    		
	    		String proName = PropertyReader.getProperty("ProjectName");
	    		if (proName.contentEquals(ProjectName.trim())) {
	    			Log.message("Project Name Verified Sucessfully");	

	    		} else {
	    			Log.message("Project name Not Verified Sucessfully");

	    		}
	    		
	    		}	    				
	    		else if(Flag.equalsIgnoreCase("Search_ProjectNumber")){
	    		SkySiteUtils.waitForElement(driver, Filter_ProjectNumber,120);
	    		Filter_ProjectNumber.sendKeys(CommonValue);
	    		SkySiteUtils.waitTill(5000);
	    		Log.message("Entered the Value In Project Number field:-"+CommonValue);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SkySiteUtils.waitTill(15000);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");		
	    		
	    		String ProjectNo = Project_NumberText.getText();	    		
	    		String[] parts = ProjectNo.split(" ");
	    		String part1 = parts[1]; 
	    		String projNo = PropertyReader.getProperty("Creator");
	    		
	    		if (part1.trim().contentEquals(CommonValue.trim())) {
	    			Log.message("Project Number Verified Sucessfully");	

	    		} else {
	    			Log.message("Project Number Not Verified Sucessfully");

	    		}
	    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_ProjectDescription")){
	    		SkySiteUtils.waitForElement(driver, Filter_Description, 120);
	    		
	    		Filter_Description.sendKeys(CommonValue);
	    		Log.message("Entered the Value In Project Description field:-"+CommonValue);
	    		SkySiteUtils.waitTill(5000);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SkySiteUtils.waitTill(40000);
	    		SearchButton_Advance.click();
	    		SkySiteUtils.waitTill(5000);	    		
	    		SkySiteUtils.waitForElement(driver, projectMultipleObject, 120);
	    		projectMultipleObject.click();
	    		SkySiteUtils.waitForElement(driver, projectMultipleObject_edit, 120);
	    		SkySiteUtils.waitTill(3000);
	    		projectMultipleObject_edit.click();
	    		SkySiteUtils.waitTill(10000);
	    		String Description = Descriptiontext.getAttribute("value");
	    		SkySiteUtils.waitForElement(driver, project_CancelButton, 120);
	    		SkySiteUtils.waitTill(3000);
	    		project_CancelButton.click();
	    		
	    		
	    		if (Description.trim().contains(CommonValue.trim())) {
	    			Log.message("Project Description Verified Sucessfully");	

	    		} else {
	    			Log.message("Project description Not Verified Sucessfully");

	    		}
	    		
	    		
	    		
	    		}
	    		
	    		
	    		else if(Flag.equalsIgnoreCase("Search_City")){
	    		Filter_CITY.sendKeys(CommonValue);	    		
	    		Log.message("Entered the Value In CITY field:-"+CommonValue);
	    		SkySiteUtils.waitTill(40000);
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");
	    		SkySiteUtils.waitTill(3000);
	    		SkySiteUtils.waitForElement(driver, projectMultipleObject, 120);
	    		projectMultipleObject.click();
	    		SkySiteUtils.waitForElement(driver, projectMultipleObject_edit, 120);
	    		SkySiteUtils.waitTill(3000);
	    		projectMultipleObject_edit.click();
	    		SkySiteUtils.waitTill(5000);
	    		String CITY = CITYtext.getAttribute("value");
	    		SkySiteUtils.waitForElement(driver, project_CancelButton, 120);
	    		SkySiteUtils.waitTill(3000);
	    		project_CancelButton.click();   		
	    		
	    		if (CITY.trim().contains(CommonValue.trim())) {
	    			Log.message("City Verified Sucessfully");	

	    		} else {
	    			Log.message("City Not Verified Sucessfully");

	    		}
	    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_State")){
		    		SkySiteUtils.waitForElement(driver, AdvanceSearch_PunchStatus, 120);
		    		AdvanceSearch_PunchStatus.sendKeys(CommonValue);
		    		Log.message("Entered the Value In Status field:-"+CommonValue);
		    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		    		SearchButton_Advance.click();
		    		Log.message("Search Button Clicked Sucessfully");	    		
		    		String status1 = PropertyReader.getProperty("Status");
		    		String status = Punchstatus_text.getText();
		    		if (status.trim().contains(status1.trim())) {
		    			Log.message("State Verified Sucessfully");	

		    		} else {
		    			Log.message("State Not Verified Sucessfully");

		    		}
		    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_Country")){
		    		SkySiteUtils.waitForElement(driver, AdvanceSearch_PunchStatus, 120);
		    		AdvanceSearch_PunchStatus.sendKeys(CommonValue);
		    		Log.message("Entered the Value In Status field:-"+CommonValue);
		    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		    		SearchButton_Advance.click();
		    		Log.message("Search Button Clicked Sucessfully");	    		
		    		String status1 = PropertyReader.getProperty("Status");
		    		String status = Punchstatus_text.getText();
		    		if (status.trim().contains(status1.trim())) {
		    			Log.message("Country Verified Sucessfully");	

		    		} else {
		    			Log.message("Country Not Verified Sucessfully");

		    		}
		    		}
	    		
	    		
	    		return result;

	    	}
	        
	        
	       public boolean AdvanceSearch_Punch(String CommonValue,String Flag) throws Throwable {
	    		boolean result = false;
	    		SkySiteUtils.waitTill(5000);
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
	    		AdvanceSearch_Link.click();
	    		Log.message("Advance Search Option Clicked Sucessfully");	
	    		if(Flag.equalsIgnoreCase("Search_Description")){
	    		
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_PunchDescription, 120);
	    		AdvanceSearch_PunchDescription.sendKeys(CommonValue);
	    		Log.message("Entered the Value In Description field:-"+CommonValue);
	    		SkySiteUtils.waitTill(40000);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");
	    		SkySiteUtils.waitTill(1000); 	    		
	    		String punch_Description = PunchDescription_text.getText();	    		
	    		String Description = PropertyReader.getProperty("Punch_Subject");
	    		if (punch_Description.contentEquals(Description.trim())) {
	    			Log.message("Description Verified Sucessfully");	

	    		} else {
	    			Log.message("Description Not Verified Sucessfully");

	    		}
	    		
	    		}	    				
	    		else if(Flag.equalsIgnoreCase("Search_Creator")){
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_PunchCreator, 120);
	    		AdvanceSearch_PunchCreator.sendKeys(CommonValue);	    		
	    		SkySiteUtils.waitTill(40000);
	    		Log.message("Entered the Value In Creator field:-"+CommonValue);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");		
	    		SkySiteUtils.waitTill(3000);
	    		/*SkySiteUtils.waitForElement(driver, Profile, 120);
	    		Profile.click();
	    		SkySiteUtils.waitForElement(driver, MYProfile, 120);
	    		MYProfile.click();*/	    		
	    		String createrText = "arc";
	    		SkySiteUtils.waitTill(5000);
	    		String Creator = PropertyReader.getProperty("Creator");
	    		if (createrText.trim().contentEquals(Creator.trim())) {
	    			Log.message("Creator Verified Sucessfully");	

	    		} else {
	    			Log.message("Creator Not Verified Sucessfully");

	    		}
	    		
	    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_Assignee")){
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_PunchAssignee, 120);
	    		AdvanceSearch_PunchAssignee.sendKeys(CommonValue);
	    		Log.message("Entered the Value In Assignee field:-"+CommonValue);
	    		SkySiteUtils.waitTill(40000);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");
	    		
	    		String Assignee1 = Assignee.getText();
	    		String [] arrOfStr = Assignee1.split(":");
	    		 
	            for (String a : arrOfStr){
	                System.out.println(a);
	                //Log.message("Assignee Verified Sucessfully---"+a);	
	    		
	    		if (a.contentEquals(CommonValue.trim())) {
	    			Log.message("Assignee Verified Sucessfully");	

	    		} else {
	    			Log.message("Assignee Not Verified Sucessfully");

	    		}	}}
	    		
	    		
	    		
	    		else if(Flag.equalsIgnoreCase("Search_Status")){
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_PunchStatus, 120);
	    		AdvanceSearch_PunchStatus.sendKeys(CommonValue);
	    		Log.message("Entered the Value In Status field:-"+CommonValue);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");	    		
	    		String status1 = PropertyReader.getProperty("Status");
	    		String status = Punchstatus_text.getText();
	    		if (status.trim().equalsIgnoreCase(status1.trim())) {
	    			Log.message("status Verified Sucessfully");	

	    		} else {
	    			Log.message("Status Not Verified Sucessfully");

	    		}
	    		}
	    		
	    		
	    		
	    		
	    		return result;

	    	}

	       public boolean AdvanceSearch_Submittal(String CommonValue,String Flag) throws Throwable {
	    		boolean result = false;
	    		SkySiteUtils.waitTill(5000);
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
	    		AdvanceSearch_Link.click();
	    		Log.message("Advance Search Option Clicked Sucessfully");
	    		
	    		if(Flag.equalsIgnoreCase("Search_SubmittalNumber")){
	    		
	    		SkySiteUtils.waitForElement(driver, Filter_SubmittalNumber, 120);
	    		Filter_SubmittalNumber.sendKeys(CommonValue);	
	    		SkySiteUtils.waitTill(40000);
	    		Log.message("Entered the Value In Submittal Number field:-"+CommonValue);	
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");	    		
	    		SkySiteUtils.waitTill(5000);
	    		String SubmittalNumber = SubmittalN0.getText();
	    		Log.message("Submittal Number is:" + SubmittalN0);
	    		
	    		if (SubmittalNumber.trim().contentEquals(CommonValue.trim())) {
	    			Log.message("Maching Submittal Number Found!!");

	    		} else {

	    			Log.message("Matching Submittal Number Not Found!!");

	    		}
	    		
	    		}	    				
	    		else if(Flag.equalsIgnoreCase("Search_SubmittalName")){
	    		SkySiteUtils.waitTill(5000);
	    		SkySiteUtils.waitForElement(driver, Filter_SubmittalName, 120);
	    		Filter_SubmittalName.sendKeys(CommonValue);	
	    		Log.message("Entered the Value In Submittal Name field:-"+CommonValue);
	    		SkySiteUtils.waitTill(40000);
	    		Log.message("Entered the Value In Creator field:-"+CommonValue);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");		
	    		SkySiteUtils.waitTill(3000);	    		    		
	    		String SubmittalName1 = SubmittalName.getText();
	    		Log.message("Submittal Number is:" + SubmittalName);
	    		if (SubmittalName1.trim().contentEquals(CommonValue.trim())) {
	    			Log.message("Maching Submittal Name Found!!");

	    		} else {

	    			Log.message("Matching Submittal Name Not Found!!");

	    		}
	    		
	    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_SubmittalType")){
	    		SkySiteUtils.waitTill(1000);
	    		//SkySiteUtils.waitForElement(driver, dropDownSubmittalType, 120);
	    		
	    		Select object = new Select(driver.findElement(By.xpath(".//*[@id='ddlSubmittalType_filter']")));
	    		object.selectByVisibleText(CommonValue);	    		
	    		SkySiteUtils.waitTill(40000);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click();
	    		SkySiteUtils.waitTill(5000);
	    		Log.message("Search Button Clicked Sucessfully");
	    		SkySiteUtils.waitForElement(driver, FirstSubmittallist, 60);
	    		FirstSubmittallist.click();
	    		SkySiteUtils.waitTill(5000);
	    		String Submittal= submittalTypeText.getText();
	    		SkySiteUtils.waitForElement(driver, CloseSubmittalwindow, 60);
	    		CloseSubmittalwindow.click();
	    		SkySiteUtils.waitTill(5000);
	    		if (Submittal.contentEquals(CommonValue.trim())) {
	    			Log.message("Submittal Type Verified Sucessfully");	

	    		} else {
	    			Log.message("Submittal Type Not Verified Sucessfully");

	    		}	}
	    		
	    		
	    		
	    		else if(Flag.equalsIgnoreCase("Search_Status")){
	    		SkySiteUtils.waitForElement(driver, Filter_SubmittalStatus, 120);
	    		
	    		Select object = new Select(Filter_SubmittalStatus);
	    		object.deselectByIndex(4);
	    		//Filter_SubmittalStatus.sendKeys(CommonValue);
	    		Log.message("Entered the Value In Status field:-"+CommonValue);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");	    		
	    		String status1 = PropertyReader.getProperty("Status");
	    		String status = submittalstatus_text.getText();
	    		if (status.trim().contains(status1.trim())) {
	    			Log.message("status Verified Sucessfully");	

	    		} else {
	    			Log.message("Status Not Verified Sucessfully");

	    		}
	    		}
	    		
	    		
	    		
	    		
	    		return result;

	    	}

	
	
	
	public boolean AdvanceSearch_Contact(String CommonValue,String flag) throws Throwable {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		Log.message("Enter Into  filter By search status");
		SkySiteUtils.waitForElement(driver, Contacts, 60);
		Contacts.click();
		Log.message("Contact Link clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");		
		if(flag.equalsIgnoreCase("Search_FirstName")){
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_FirstName, 120);
		AdvanceSearch_FirstName.sendKeys(CommonValue);
		Log.message("Entered the Value In First Name field:-"+CommonValue);	
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(1000);
		String Cont_FirstName = Name.getText();
		String[] parts = Cont_FirstName.split(" ");
		String part1 = parts[0]; // 004
		SkySiteUtils.waitTill(1000);
		String Contact_FirstName = PropertyReader.getProperty("Contact_FirstName");
		if (part1.trim().contentEquals(Contact_FirstName.trim())) {
			Log.message("First Name Verified Sucessfully");	

		} else {
			Log.message("First Name Not Verified Sucessfully");

		}
		}
				
		else if(flag.equalsIgnoreCase("Search_LastName")){
		SkySiteUtils.waitTill(5000);
		AdvanceSearch_LastName.sendKeys(CommonValue);
		SkySiteUtils.waitTill(5000);
		Log.message("Entered the Value In Last Name field:-"+CommonValue);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");		
		SkySiteUtils.waitTill(5000);
		String Cont_FirstName = Name.getText();
		String[] parts = Cont_FirstName.split(" ");
		String part1 = parts[1]; // 004
		String Contact_LastName = PropertyReader.getProperty("Contact_LastName");
		if (part1.trim().contentEquals(Contact_LastName.trim())) {
			Log.message("Last Name Verified Sucessfully");	

		} else {
			Log.message("Last Name Not Verified Sucessfully");

		}
		
		}
		
		else if(flag.equalsIgnoreCase("Search_EMAIL")){
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		AdvanceSearch_Email.sendKeys(CommonValue);
		Log.message("Entered the Value In Email field:-"+CommonValue);
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		
		/*String Cont_email = EmailText.getText();
		if (Cont_email.trim().contains(CommonValue.trim())) {
			Log.message("Email Verified Sucessfully");	

		} else {
			Log.message("Email Not Verified Sucessfully");

		}*/
		
		
		
		
		
		
		
		}
		
		else if(flag.equalsIgnoreCase("Search_PHONE")){
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Phone, 120);
		AdvanceSearch_Phone.sendKeys(CommonValue);
		Log.message("Entered the Value In Phone field:-"+CommonValue);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		
		
		/*String Cont_phone = phoneText.getText();
		if (Cont_phone.trim().contains(CommonValue.trim())) {
			Log.message("Phone Verified Sucessfully");	

		} else {
			Log.message("Phone Not Verified Sucessfully");

		}*/
		}
		
		else if(flag.equalsIgnoreCase("Search_CITY")){
		SkySiteUtils.waitForElement(driver, AdvanceSearch_City, 120);
		AdvanceSearch_City.sendKeys(CommonValue);
		Log.message("Entered the Value In Phone field:-"+CommonValue);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		}
		else if(flag.equalsIgnoreCase("Search_Address")){
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Address, 120);
		AdvanceSearch_Address.sendKeys(CommonValue);
		Log.message("Entered the Value In Address field:-"+CommonValue);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		}
		
		
		
		return result;

	}

	public boolean RFI_AdvanceSearchWithRFINumber() throws Throwable {
		boolean result = false;
		Log.message("Enter Into  filter By search status");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");
		String RFINUMBER1 = RFI_Number.getText();
		SkySiteUtils.waitForElement(driver, Filter_RFINumber, 60);
		Filter_RFINumber.sendKeys(RFINUMBER1);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 60);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		String RFINUMBER2 = RFI_Number.getText();
		if (RFINUMBER1.trim().contentEquals(RFINUMBER2.trim())) {
			Log.message("RFI Number Verified Sucessfully");

			SkySiteUtils.waitTill(5000);

		} else {

			Log.message("RFI Number Not Verified Sucessfully");

		}

		return result;

	}
	
	
	public boolean Submital_Create() throws Throwable {
		boolean result = false;
		
		
		
		
		return result;
	
	}
	

	
	public boolean Submital_ModuleSearch() throws Throwable {
		boolean result = false;
		
		
		
		
		return result;
	
	}
	
	
	public boolean RFI_AdvanceSearchWithRFIDueDate() throws Throwable {
		boolean result = false;
		Log.message("Enter Into  filter By search status");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		String duedate = RFI_Duedate_Text.getText();
		Log.message("Due date"+duedate);

		String[] date = duedate.split("/S/");
		int dateIndex = 1;
		for (String Dates : date) {
			Log.message(dateIndex + ". " + date);

			dateIndex++;
		}

		String textStr[] = duedate.split("\\s", 10);
		String myDate = textStr[4] + " " + textStr[5] + " " + textStr[6];
		Log.message(textStr[0]);
		Log.message(textStr[1]);
		Log.message(textStr[2]);
		Log.message(myDate);

		Calendar cal = Calendar.getInstance();
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		 String dayOfMonthStr = String.valueOf(dayOfMonth);
		
	

		// String dayOfMonthStr = String.valueOf(dayOfMonth +2);
		// Log.message(dayOfMonthStr);
		// driver.findElement(By.xpath(".//*[@class='day' and text()='dayOfMonthStr']"));

		// switch statement with int data type
	/*	switch (dayOfMonth) {
		case 1:
			dayOfMonth = 30;
			if (new31.isDisplayed()) {

				dayOfMonth = 1;
			} else {
				dayOfMonth = 2;
			}
			break;
		case 2:
			dayOfMonth = 28;
			if (!feb29.isDisplayed()) {

				dayOfMonth = 2;
			} else {
				dayOfMonth = 1;
			}
			break;

		default:
			dayOfMonth = dayOfMonth+2;
			break;
		}
*/
		/*
		 * SkySiteUtils.waitForElement(driver,Filter_RFINumber, 60);
		 * Filter_RFIDueDate.sendKeys(RFINUMBER1);
		 * SkySiteUtils.waitForElement(driver,SearchButton_Advance, 60);
		 * SearchButton_Advance.click();
		 * Log.message("Search Button Clicked Sucessfully"); String RFINUMBER2 =
		 * RFI_Number.getText();
		 * if(RFINUMBER1.trim().contentEquals(RFINUMBER2.trim())) {
		 * Log.message("RFI Number Verified Sucessfully");
		 * 
		 * 
		 * SkySiteUtils.waitTill(5000);
		 * 
		 * } else {
		 * 
		 * Log.message("RFI Number Not Verified Sucessfully");
		 * 
		 * }
		 */
		return result;

	}

	public boolean AdvanceSearch_Description() throws Throwable {
		boolean result = false;
		Log.message("Enter Into  filter By search status");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, FilterProject_Name, 60);
		String Anything = Punch_Text.getText();
		String description = PropertyReader.getProperty("Punch_Subject");
		filterSearch_DescriptionName.sendKeys(Anything);
		Log.message("Entered The Value In search Inbox Text Field: - " + Description);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 60);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		if (Anything.trim().contentEquals(description.trim())) {
			Log.message("Maching Punch  Found!!");

			SkySiteUtils.waitTill(5000);

		} else {

			Log.message("Matching Punch Not Found!!");

		}

		return result;

	}
	
	public boolean CreateProject(String Project_Name,String Project_Number,String Description,String Country,String City,String State )
	{   
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnCreateProject,60);		
		btnCreateProject.click();
		Log.message("Create Project button is clicked.");
		SkySiteUtils.waitTill(3000);
		txtProjectName.clear();
		txtProjectName.sendKeys(Project_Name);
		Log.message("Project Name has been entered as ."+Project_Name);
		txtProjectNumber.clear();
		txtProjectNumber.sendKeys(Project_Number);
		Log.message("Project Number has been entered.");
		txtProjectDescription.clear();
		txtProjectDescription.sendKeys(Description);
		Log.message("Project description has been entered.");
		//txtProjectStartDate.clear();
		txtProjectStartDate.sendKeys(PropertyReader.getProperty("Projectstartdate"));
		Log.message("Project start date has been entered.");
		txtAddress.clear();
		txtAddress.sendKeys(PropertyReader.getProperty("Siteaddress"));
		Log.message("Site Address has been entered.");
		txtCountry.clear();
		txtCountry.sendKeys(Country);
		SkySiteUtils.waitForElement(driver, txtCountry,100);
		SkySiteUtils.waitTill(500);
		countryTitleItem.click();
		Log.message("Country has been selected.");
		SkySiteUtils.waitTill(3000);
		txtState.clear();
		SkySiteUtils.waitTill(3000);
		txtState.sendKeys(State);
		SkySiteUtils.waitForElement(driver, stateTitleItem, 60);
		stateTitleItem.click();
		Log.message("State has been selected.");
		txtCity.clear();
		txtCity.sendKeys(City);
		Log.message("City has been entered.");
		txtZip.clear();
		txtZip.sendKeys(PropertyReader.getProperty("Zip"));
		Log.message("Zip has been entered.");
		SkySiteUtils.waitTill(2000);
		btnCreate.click();
		Log.message("Create Project button has been clicked.");	
		SkySiteUtils.waitForElement(driver, checkboxEnableAutoHypLink,120);
		SkySiteUtils.waitTill(1000);
		checkboxEnableAutoHypLink.click();
		Log.message("Clicked on enable auto hyperlink checkbox.");
		SkySiteUtils.waitTill(2000);
		btnSavePrjSettingsWin.click();
		Log.message("Clicked on save button of project settings window.");
		SkySiteUtils.waitForElement(driver, btnYes,120);
		SkySiteUtils.waitTill(2000);
		btnYes.click();
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, projectDasboadLink,120);		
		projectDasboadLink.click();
		
		return result;
		
	}

	

	@FindBy(css = "#add-folder")
	WebElement btnAddNewAlbum;
	@FindBy(css = ".btn.btn-default.pull-left")
	WebElement btnCancelAddAlbumWind;
	@FindBy(css = "#txtAlbumName")
	WebElement txtAlbumName;
	@FindBy(xpath = "(//button[@class='btn btn-default'])[4]")
	WebElement btnCreateAlbum;
	@FindBy(xpath = "(//button[@class='btn btn-primary btn-lg'])[2]")
	WebElement btnUploadPhotos;

	public boolean Verify_AddAlbum(String Album_Name) throws InterruptedException, AWTException, IOException {
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnAddNewAlbum, 120);
		Log.message("Add New Album button is available.");
		btnAddNewAlbum.click();
		Log.message("Click on Add New Album");
		SkySiteUtils.waitForElement(driver, btnCancelAddAlbumWind, 120);
		txtAlbumName.click();
		SkySiteUtils.waitTill(3000);
		txtAlbumName.sendKeys(Album_Name);
		Log.message("Entered Album Name.:-" + Album_Name);
		SkySiteUtils.waitTill(3000);
		btnCreateAlbum.click();
		Log.message("Clicked on Create Album button.");
		SkySiteUtils.waitForElement(driver, notificationMsg, 20);
		String ActualMsg = notificationMsg.getText();
		Log.message("Notify message after create an Album is: " + ActualMsg);
		SkySiteUtils.waitTill(5000);
		// Counting available Albums
		int Album_Count = 0;
		int i = 1;
		List<WebElement> allElements = driver.findElements(By.xpath("//section[1]/i"));
		for (WebElement Element : allElements) {
			Album_Count = Album_Count + 1;
		}
		Log.message("Available Albums in a gallery is: " + Album_Count);
		// Getting Album Names and matching
		for (i = 1; i <= Album_Count; i++) {
			String Act_AlbumName = driver
					.findElement(By.xpath("html/body/div[1]/div[4]/div[2]/div/div/div/ul/li[" + i + "]/section[2]/h4"))
					.getText();
			if ((Act_AlbumName.contentEquals(Album_Name)) && (ActualMsg.contentEquals("Album successfully created."))) {
				result1 = true;
				break;
			}
		}
		if (result1 == true) {
			Log.message("Add Album is working successfully!!!");
			return true;
		} else {
			Log.message("Add Album is NOT working!!!");
			return false;
		}
	}

	public boolean AdvanceStatus(String StatusDispaly) throws Throwable {
		boolean result = true;
		Log.message("Enter Into  filter By search status");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 60);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Status_Inbox, 60);
		SkySiteUtils.waitTill(3000);
		Status_Inbox.sendKeys(StatusDispaly);
		Log.message("Entered The Value In search Inbox Text Field: - " + StatusDispaly);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 60);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(3000);
		return result;

	}

	public boolean SearchRFI_RFINumber(String RFINumber) throws Throwable

	{
		boolean result = true;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Global Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		Everywhere_AllContents.click();
		Log.message("Everywhere AllContents Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		SkySiteUtils.waitTill(5000);
		TxtSearch_Keyword.sendKeys(RFINumber);
		Log.message("Enter The Value In search Inbox Field: - " + RFINumber);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		return result;

	}

	public boolean SearchProjectWithSubject(String Sub_Name) throws Throwable

	{
		boolean result = true;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Global Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		Everywhere_AllContents.click();
		Log.message("Everywhere AllContents Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		SkySiteUtils.waitTill(5000);
		TxtSearch_Keyword.sendKeys(Sub_Name);
		Log.message("Enter The Value In search Inbox Field: - " + Sub_Name);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		return result;

	}

}