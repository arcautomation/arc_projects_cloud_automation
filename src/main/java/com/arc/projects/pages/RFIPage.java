package com.arc.projects.pages;

import java.awt.AWTException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;


public class RFIPage extends LoadableComponent<RFIPage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	
	/** Identifying web elements using FindBy annotation*/
	@FindBy (xpath="//*[@id='rfilistContainer']/div[1]/div[1]/ul/li/a")
	WebElement btnCreateRFI;
	
	@FindBy (xpath="//*[@id='btnCreateRFI']")
	WebElement btnCreateRFIPopover;
	
	@FindBy (xpath="//*[@id='divAddRFIUI']/div/div[2]/div[2]/div[1]/div[2]/div/div/a/i")
	WebElement btnAddRFIToUser;
	
	@FindBy (xpath="//*[@id='divProjectUsers']/div/div[3]/button[2]")
	WebElement btnSelectUser;
	
	@FindBy (xpath="//*[@id='divUserListView']/ul/li[2]")
	WebElement RFIToUser;
	
	@FindBy (xpath="//*[@id='txtSubject']")
	WebElement textboxRFISubject;
	
	@FindBy (xpath="//*[@id='txtQuestion']")
	WebElement textboxRFIQuestion;
	
	@FindBy (xpath="//*[@id='ulRFIList']/li[1]/div[2]/div[1]/div/h4/span")
	WebElement createdRFI;
	
	@FindBy (xpath="//*[@id='liActivityMain']/a/span[1]")
	WebElement btnProjectActivity;
	
	@FindBy (xpath="//*[@id='liActivity']/a/span")
	WebElement btnDocumentActivity;
	
	@FindBy (xpath="//*[@id='ulRFIList']/li[1]/div[2]/div[1]/div/h5/span[1]")
	WebElement createdRFINo;
	
	
	@Override
	protected void load() 
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnCreateRFI, 20);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * 
	 * @param driver
	 * @return
	 */
	public RFIPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	/** 
	 * Method for creating RFI for Activity tracking
	 * Scripted By: Ranadeep
	 * @return 
	 */
	
	public ProjectActivitiesPage createRFI()	 
	{
		
		SkySiteUtils.waitForElement(driver, btnCreateRFI, 20);
		
		WebElement element = driver.findElement(By.xpath("//*[@id='rfilistContainer']/div[1]/div[1]/ul/li/a"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
	    Log.message("Clicked on the create RFI button");
	    
	    SkySiteUtils.waitForElement(driver, btnAddRFIToUser, 20);
	    btnAddRFIToUser.click();
	    Log.message("Clicked to open the add RFI to user popup");	    
	    
	    SkySiteUtils.waitForElement(driver, RFIToUser, 20);
	    RFIToUser.click();
	    Log.message("Selected the to RFI user");
	    
	    WebElement element1 = driver.findElement(By.xpath("//*[@id='divProjectUsers']/div/div[3]/button[2]"));
	    JavascriptExecutor executor1 = (JavascriptExecutor)driver;
	    executor1.executeScript("arguments[0].click();", element1);
	    Log.message("Clicked on the Select user button");
	    
	    SkySiteUtils.waitForElement(driver, textboxRFISubject, 20);
	    String expectedSubjectRFI = PropertyReader.getProperty("SUBJECTRFI");
	    textboxRFISubject.clear();
	    textboxRFISubject.sendKeys(expectedSubjectRFI);
		Log.message("RFI subject entered is:- " +expectedSubjectRFI);
		
		SkySiteUtils.waitForElement(driver, textboxRFIQuestion, 20);
		String questionRFI = PropertyReader.getProperty("QUESTIONRFI");
		textboxRFIQuestion.clear();
		textboxRFIQuestion.sendKeys(questionRFI);
		Log.message("RFI Question entered is:- " +questionRFI);
		
		SkySiteUtils.waitForElement(driver, btnCreateRFIPopover, 20);
		btnCreateRFIPopover.click();
		Log.message("Clicked on Create RFI button");
		
		SkySiteUtils.waitTill(8000);
		String actualSubjectRFI=createdRFI.getText();
		Log.message("Created RFI subject is:- " +actualSubjectRFI);
		
		if(actualSubjectRFI.equals(expectedSubjectRFI))
		{
			Log.message("RFI created successfully");
			
			SkySiteUtils.waitForElement(driver, btnProjectActivity, 20);
			WebElement element2 = driver.findElement(By.xpath("//*[@id='liActivityMain']/a/span[1]"));
		    JavascriptExecutor executor2 = (JavascriptExecutor)driver;
		    executor2.executeScript("arguments[0].click();", element2);
			Log.message("Clicked on Project Activity");
			
			SkySiteUtils.waitForElement(driver, btnDocumentActivity, 20);
			WebElement element3 = driver.findElement(By.xpath("//*[@id='liActivity']/a/span"));
		    JavascriptExecutor executor3 = (JavascriptExecutor)driver;
		    executor3.executeScript("arguments[0].click();", element3);
			Log.message("Clicked on Document Activity");			
			
			return new ProjectActivitiesPage(driver).get();
			
			
		}		
		
		else
		{
				Log.message("RFI creation failed");
				return null;
		}	
		
	}
	
	
}
