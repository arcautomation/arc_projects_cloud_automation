package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import com.arc.projects.pages.ProjectGalleryPage;


public class PunchPage extends LoadableComponent<PunchPage>
{
	
	WebDriver driver;
	private boolean isPageLoaded;
	
	/** Identifying web elements using FindBy annotation*/
	
	
	@FindBy(xpath="//*[@id='createNewPunch']")
	WebElement btnCreatePunch;
	
	@FindBy(xpath="//*[@id='btnCreate']")
	WebElement btnCreatePunchPopover;
	
	@FindBy(xpath="//*[@id='closeAddNewPunchStamp']")
	WebElement btnSelectPunchStampDropdown;
	
	@FindBy(xpath="//*[@id='ulStampList']/li/a/label")
	WebElement btnSelectPunchStamp;
	
	@FindBy(xpath="//*[@id='txtShortDescription']")
	WebElement textBoxStamp;
	
	@FindBy(xpath="//*[@id='txtTitleDescription']")
	WebElement textBoxStampTitle;
	
	@FindBy(xpath="//*[@id='divaddPunchTitle']/div/div[2]/div/span/button[1]")
	WebElement btnCreatePunchStampOK;
	
	@FindBy(xpath="//*[@id='divPunchAssignee']/div/a/i")
	WebElement btnAssignPunchDropdown;
	
	@FindBy(xpath="//*[@id='divProjectUsers']/div/div[3]/button[2]")
	WebElement btnSelectAssignPunchUser;
	
	@FindBy(xpath="//*[@id='divUserListView']/ul/li")
	WebElement selectAssignPunchUser;
	
	@FindBy(xpath="//*[@id='punchDueDate']/span/span")
	WebElement punchDueDate;
	
	@FindBy(xpath="//*[@class='day active today']")
	WebElement activePunchDate;
	
	@FindBy(xpath="//*[@id='ulPunchList']/li[1]/div[2]/div[1]/div[2]/h4/span[1]")
	WebElement punchTitle;
	
	@FindBy(xpath="//*[@id='liActivityMain']/a/span[1]")
	WebElement btnProjectActivity;
	
	@FindBy(xpath="//*[@id='liActivity']/a/span")
	WebElement btnDocumentActivity;
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnCreatePunch, 20);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * 
	 * @param driver
	 * @return
	 */
	public PunchPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	/** 
	 * Method for creating Punch for Activity tracking
	 * Scripted By: Ranadeep
	 * @return 
	 */
	
	public ProjectActivitiesPage createPunch()	 
	{
		
		WebElement element = driver.findElement(By.xpath("//*[@id='createNewPunch']"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on create punch button");
		
		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 40);
		btnSelectPunchStampDropdown.click();
		Log.message("Clicked on select Punch Stamp button dropdown");
		
		WebElement element1 = driver.findElement(By.xpath("//*[@id='ulStampList']/li/a/label"));
	    JavascriptExecutor executor1 = (JavascriptExecutor)driver;
	    executor1.executeScript("arguments[0].click();", element1);
		Log.message("Punch stamp selected");
		
		WebElement element2 = driver.findElement(By.xpath("//*[@id='divPunchAssignee']/div/a/i"));
	    JavascriptExecutor executor2 = (JavascriptExecutor)driver;
	    executor2.executeScript("arguments[0].click();", element2);
		Log.message("Clicked on assign Punch user dropdown button");	
		
		SkySiteUtils.waitForElement(driver, selectAssignPunchUser, 20); 
		WebElement element3 = driver.findElement(By.xpath("//*[@id='divUserListView']/ul/li"));
	    JavascriptExecutor executor3 = (JavascriptExecutor)driver;
	    executor3.executeScript("arguments[0].click();", element3);
		Log.message("Selected assign Punch user");
		
		SkySiteUtils.waitForElement(driver, btnSelectAssignPunchUser, 20);
		btnSelectAssignPunchUser.click();
		Log.message("Clicked on Select User button");
		
		SkySiteUtils.waitForElement(driver, punchDueDate, 20);
		punchDueDate.click();
		Log.message("Clicked to select punch due date");
		
		SkySiteUtils.waitForElement(driver, activePunchDate, 20);
		activePunchDate.click();
		Log.message("Punch due date selected");
		
		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 20);
		btnCreatePunchPopover.click();
		Log.message("Clicked on create Punch button");
		SkySiteUtils.waitTill(8000);
		
		
		String expectedPunchStampTitle = PropertyReader.getProperty("StampTitle");
		Log.message("Expected Stamp Title is:-" + expectedPunchStampTitle);
		
		SkySiteUtils.waitForElement(driver, punchTitle, 20);
		String actualPunchTitle=punchTitle.getText();
		Log.message("The created punch title is: " +actualPunchTitle);
		
		if(actualPunchTitle.equals(expectedPunchStampTitle))
		{
			Log.message("Punch created successfully");
			
			SkySiteUtils.waitForElement(driver, btnProjectActivity, 20);
			WebElement element4 = driver.findElement(By.xpath("//*[@id='liActivityMain']/a/span[1]"));
		    JavascriptExecutor executor4 = (JavascriptExecutor)driver;
		    executor4.executeScript("arguments[0].click();", element4);
			Log.message("Clicked on Project Activity button");
			
			SkySiteUtils.waitForElement(driver, btnDocumentActivity, 20);
			WebElement element5 = driver.findElement(By.xpath("//*[@id='liActivity']/a/span"));
		    JavascriptExecutor executor5 = (JavascriptExecutor)driver;
		    executor5.executeScript("arguments[0].click();", element5);
			Log.message("Clicked on Document Activity button");
			
			
			return new ProjectActivitiesPage(driver).get();	
		}		
		else
		{
			Log.message("Punch creation failed");
			return null;
		}
		
			
	}
	

}

	

