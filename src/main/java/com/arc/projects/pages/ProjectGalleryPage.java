package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import com.arc.projects.pages.ProjectGalleryPage;
import com.arc.projects.pages.ProjectActivitiesPage;

public class ProjectGalleryPage extends LoadableComponent<ProjectGalleryPage> {
	
	WebDriver driver;
	private boolean isPageLoaded;


	/** Identifying web elements using FindBy annotation*/

	@FindBy(css = "button.btn:nth-child(5)")
	WebElement buttonUploadPhotos;
	
	
	@FindBy(css = "#liActivityMain > a:nth-child(1) > span:nth-child(3)")
	WebElement btnProjectActivity;
	
	@FindBy(css = "#liActivity > a:nth-child(1) > span:nth-child(3)")
	WebElement btnDocumentActivity;
	
	@FindBy(css = ".myDropdown > a:nth-child(1) > i:nth-child(1)")
	WebElement btnPhotoOptions;
	
	@FindBy(css = ".myDropdown > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1)")
	WebElement btnPhotoDelete;
	
	
	@FindBy(xpath = "//*[@id=\"button-1\"]")
	WebElement btnDeletePhotoYes;
	
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, buttonUploadPhotos, 20);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * 
	 * @param driver
	 * @return
	 */
	public ProjectGalleryPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}



	public UploadPhotosPage uploadPhotosClick() {
		
		//SkySiteUtils.waitForElement(driver, buttonUploadPhotos, 30);
		 SkySiteUtils.waitTill(3000);  
		buttonUploadPhotos.click();
		Log.message("Clicked on Upload Photos button");      
		
		return new UploadPhotosPage(driver).get();
	}

	public boolean deletePhoto() {
		
		SkySiteUtils.waitTill(3000);
		btnPhotoOptions.click();
		Log.message("Clicked on Photos options button");
		SkySiteUtils.waitTill(3000);
		btnPhotoDelete.click();
		Log.message("Clicked on Delete option");
		SkySiteUtils.waitTill(3000);
		btnDeletePhotoYes.click();
		Log.message("Clicked on YES option in the Delete Photo popover");
		SkySiteUtils.waitTill(5000);
		
		if (buttonUploadPhotos.isDisplayed()) {
			Log.message("Photo is deleted");
			return true;
		} 
	 
	 else{
			Log.message("Photo delete failed");
			return false;
		}
		
	}
	
	
	public ProjectActivitiesPage documentActivity() {
		
		//SkySiteUtils.waitForElement(driver, buttonUploadPhotos, 30);
		 SkySiteUtils.waitTill(3000);  
		 btnProjectActivity.click();
		 Log.message("Clicked on Project Activity button");
		 SkySiteUtils.waitTill(3000);
		 btnDocumentActivity.click();
		 Log.message("Clicked on Document Activity button");      
		
		 return new ProjectActivitiesPage(driver).get();
	}
	
}
